import { callGraphql } from './callGraphql';
import {
  SQL_ACCOUNT_KYC,
  SQL_CLIENT_REGISTER,
  SQL_DETECT_QR_CODE,
  SQL_FIND_ACCOUNT,
  SQL_GET_BALANCE,
  SQL_GET_MERCHANT_INFO,
  SQL_GET_TRANSACTION_INFO_PAY,
  SQL_INIT_ACCOUNT,
  SQL_PAY,
  SQL_PAYMENT_MEHTOD,
  SQL_SETTING,
  SQL_UNLINK_ACCOUNT,
} from './sqls';

const handleResponse = (response) => {
  if (!response.data?.errors) {
    return { status: true, response: response?.data?.data ?? {} };
  } else {
    return { status: false, response: response?.data?.errors ?? {} };
  }
};

export const clientRegister = async (params, keys) => {
  const clientRegisterInput = {
    platform: 'EXPO',
    deviceId: params.deviceId,
    channel: 'channel',
    version: '1.0.0',
    isRoot: false,
  };
  const res = await callGraphql(
    SQL_CLIENT_REGISTER,
    {
      clientRegisterInput,
    },
    keys
  );
  return handleResponse(res);
};

export const accountInit = async (params, keys) => {
  const initInput = {
    appToken: params.appToken,
    connectToken: params.connectToken,
    clientId: params.clientId,
  };
  const res = await callGraphql(
    SQL_INIT_ACCOUNT,
    {
      initInput,
    },
    keys
  );
  return handleResponse(res);
};

export const unlink = async (params, keys) => {
  const input = {
    appToken: params.appToken,
    connectToken: params.connectToken,
  };
  const res = await callGraphql(
    SQL_UNLINK_ACCOUNT,
    {
      input,
    },
    keys
  );
  return handleResponse(res);
};

export const getWalletInfo = async (params, keys) => {
  const res = await callGraphql(SQL_GET_BALANCE, {}, keys);
  return handleResponse(res);
};

export const findAccount = async (params, keys) => {
  const res = await callGraphql(
    SQL_FIND_ACCOUNT,
    params.phone ? { accountPhone: params.phone } : {},
    keys
  );
  return handleResponse(res);
};

export const getSettingServiceMain = async (params, keys) => {
  const res = await callGraphql(
    SQL_SETTING,
    { configsAppId: params?.appId },
    keys
  );
  return handleResponse(res);
};

export const getPaymentMethod = async (params, keys) => {
  const PaymentMethodInput = {
    serviceType: 'OPEN_EWALLET_PAYMENT',
    extraData: {
      storeId: params?.storeId,
    },
    payCode: params?.payCode,
  };
  const res = await callGraphql(
    SQL_PAYMENT_MEHTOD,
    { PaymentMethodInput },
    keys
  );
  return handleResponse(res);
};

export const updateKycAccount = async (kycInput = {}, keys) => {
  const res = await callGraphql(SQL_ACCOUNT_KYC, { kycInput }, keys);
  return handleResponse(res);
};

export const detectDataQRCode = async (params = {}, keys) => {
  const res = await callGraphql(SQL_DETECT_QR_CODE, { ...params }, keys);
  return handleResponse(res);
};

export const getMerchantInfo = async (params = {}, keys) => {
  const res = await callGraphql(
    SQL_GET_MERCHANT_INFO,
    {
      getInfoMerchantInput: params?.storeId
        ? {
            storeId: params?.storeId,
            appId: params?.appId,
          }
        : {
            appId: params?.appId,
          },
    },
    keys
  );
  return handleResponse(res);
};

export const payVNPay = async (params = {}, keys) => {
  const res = await callGraphql(
    SQL_PAY,
    {
      payInput: {
        clientId: params.clientId,
        amount: params.amount,
        storeId: params.storeId,
        orderId: params.orderId,
        note: params.note,
        payment: {
          bankQRCode: {
            active: true,
            redirectUrl: params.redirectUrl,
          },
        },
      },
    },
    keys
  );
  return handleResponse(res);
};

export const getTransactionVNPay = async (params = {}, keys) => {
  console.log('----param', params);
  const res = await callGraphql(
    SQL_GET_TRANSACTION_INFO_PAY,
    {
      getTransactionInfoInput: {
        transaction: params.transaction,
      },
    },
    keys
  );
  return handleResponse(res);
};
