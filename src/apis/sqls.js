export const SQL_CLIENT_REGISTER = `mutation ClientRegister ($clientRegisterInput: ClientRegisterInput!){
  Client {
  Register(input: $clientRegisterInput) {
    succeeded
    message
    clientId
  }
}
}`;

export const SQL_INIT_ACCOUNT = `mutation AccountInitMutation($initInput: CheckInitInput) {
  OpenEWallet {
    Init(input: $initInput) {
      succeeded
      message
      handShake
      accessToken
      updateToken
      kyc {
        kycId
        state
        reason
      }
      phone
      appEnv
      storeName
      storeImage
      fullnameKyc
    }
  }
}`;

export const SQL_UNLINK_ACCOUNT = `mutation Mutation($input: UnlinkAccountInput) {
  OpenEWallet {
    Unlink(input: $input) {
      succeeded
      message
    }
  }
}
`;

export const SQL_GET_BALANCE = `query Query {
  Wallet {
    balance
    cash
    lockCash
  }
}`;

export const SQL_FIND_ACCOUNT = `query Query($accountPhone: String) {
  Account(phone: $accountPhone) {
    accountId
    fullname
    alias
    phone
    avatar
    email
    gender
    isVerifiedEmail
    isWaitingEmailVerification
    birthday
    address {
      street
      city {
        title
        identifyCode
      }
      district {
        title
        identifyCode
      }
      ward {
        title
        identifyCode
      }
    }
    kyc {
      kycId
      state
      reason
      identifyNumber
      details {
        identifyNumber
        issuedAt
      }
    }
  }
}`;

export const SQL_SETTING = `query Query ($configsTags: String, $configsAppId: String, $configsKeys: [String]){
  Setting {
    configs (tags: $configsTags, appId: $configsAppId, keys: $configsKeys){
      key
      value
      tags
    }
  }
}`;

export const SQL_PAYMENT_MEHTOD = `mutation Mutation($PaymentMethodInput: PaymentMethodInput) {
  Utility {
    GetPaymentMethod(input: $PaymentMethodInput) {
      succeeded
      message
      methods {
        methodId
        title
        label
        type
        fee
        feeDescription
        minFee
        data {
          ... on LinkedMethodInfo {
            linkedId
            issuer
            swiftCode
          }
          ... on WalletMethodInfo {
            accountId
          }
        }
      }
    }
  }
}`;

export const SQL_ACCOUNT_KYC = `mutation Mutation($kycInput: KYCInput!) {
  Account {
    KYC(input: $kycInput) {
      succeeded
      message
      createdAt
    }
  }
}`;

export const SQL_DETECT_QR_CODE = `mutation DetectDataQRCode($input: OpenEWalletPaymentDetectInput!) {
  OpenEWallet {
    Payment {
      Detect (input: $input) {
        succeeded
        message
        type
        storeId
        action
        amount
        note
        orderId
        userName
      }
    }
  }
}`;

export const SQL_GET_MERCHANT_INFO = `mutation Mutation($getInfoMerchantInput: OpenEWalletGetInfoMerchantInput!) {
  OpenEWallet {
    GetInfoMerchant(input: $getInfoMerchantInput) {
      succeeded
      message
      merchantName
      brandName
      backgroundColor
      storeImage
      storeName
      isVisibleHeader
    }
  }
}`;

export const SQL_PAY = `mutation Mutation($payInput: OpenEWalletPaymentPayInput!) {
  OpenEWallet {
    Payment {
      Pay(input: $payInput) {
        succeeded
        message
        payment {
          ... on PaymentBankTransferResponsed {
            bankTranferState: state
            message
            bankList {
              bankName
              bankCity
              bankBranch
              bankAccountName
              bankAccountNumber
              content
              swiftCode
              qrContent
              vietQRAccepted
            }
            paymentInfo {
              id
              amount
            }
          }
          ... on PaymentWalletResponsed {
            walletState: state
            message
            accountId
          }
          ... on PaymentBankQRCodeResponsed {
            bankQRCodeState: state
            message
            qrContent
            url
          }
          ... on PaymentBankCardResponsed {
            bankCardState: state
            message
            html
          }
          ... on PaymentLinkedResponsed {
            linkedState: state
            message
            linkedId
            transaction
            html
          }
          ... on PaymentCreditCardResponsed {
            creditCardState: state
            message
            transaction
            html
          }
          ... on PaymentPaymeCreditResponsed {
            paymeCredit: state
            message
            credit {
              used
              remain
            }
            methodData {
              paymeCreditId
              accountInfo {
                fullname
                accountId
              }
            }
          }
        }
        history {
          display {
            wallet {
              display1
              display2
              display3
              display4
              display5 {
                code
                title
              }
            }
            openApiWallet {
              display1
              display2
              display3
              display4
              display5 {
                code
                title
              }
            }
            paymecredit {
              display1
              display2
              display3
              display4
              display5 {
                code
                title
              }
            }
          }
          service {
            transaction
            type
            method
            state
          }
          description
          title
          amount
          fee
          total
          createdAt
          payment {
            state
            transaction
            details {
              methodData {
                ... on PaymentWalletObject {
                  paymentWalletId
                }
                ... on PaymentBankCardObject {
                  paymentBankCardId
                  bankInfo {
                    swiftCode
                    cardNumber
                    cardHolder
                    bankAccountName
                    bankAccountNumber
                    issuedDate
                  }
                }
                ... on PaymentLinkedObject {
                  paymentLinkedId
                  linkedId
                  bankInfo {
                    swiftCode
                    cardNumber
                    bankAccountNumber
                    cardHolder
                    issuedDate
                    bankAccountName
                  }
                }
                ... on PaymentCreditCardObject {
                  paymentCreditCardId
                  cardInfo {
                    cardNumber
                    cvv
                    expiredAt
                  }
                }
                ... on PaymeCreditPaymentObject {
                  paymeCreditId
                  credit {
                    remain
                    used
                  }
                }
                ... on PaymentBankQRCode {
                  paymentQRCodeId
                  qrContent
                }
              }
            }
          }
        }
      }
    }
  }
}`;

export const SQL_GET_TRANSACTION_INFO_PAY = `mutation Mutation($getTransactionInfoInput: GetTransactionInfoInput!) {
  OpenEWallet {
    Payment {
      GetTransactionInfo(input: $getTransactionInfoInput) {
        succeeded
        message
        transaction
        storeName
        reason
        amount
        state
        total
        fee
        description
        updatedAt
        createdAt
        payment {
          state
          description
          method
          details {
            methodData {
              ... on PaymentWalletObject {
                paymentWalletId
              }
              ... on PaymentBankCardObject {
                paymentBankCardId
                bankInfo {
                  swiftCode
                  cardNumber
                  cardHolder
                  bankAccountNumber
                  bankAccountName
                  issuedDate
                }
              }
              ... on PaymentBankAccountObject {
                paymentBankAccountId
                bankInfo {
                  swiftCode
                  cardNumber
                  cardHolder
                  bankAccountNumber
                  bankAccountName
                  issuedDate
                }
              }
              ... on PaymentBankCardPGObject {
                paymentBankCardPGId
                issuedDate
                cardHolder
                cardNumber
              }
              ... on PaymentLinkedObject {
                paymentLinkedId
                linkedId
                bankInfo {
                  swiftCode
                  cardNumber
                  cardHolder
                  bankAccountNumber
                  bankAccountName
                  issuedDate
                }
              }
              ... on PaymentCreditCardObject {
                paymentCreditCardId
                cardInfo {
                  cardNumber
                  expiredAt
                  cvv
                }
              }
              ... on PaymeCreditPaymentObject {
                paymeCreditId
                credit {
                  used
                  remain
                }
              }
              ... on PaymentBankCardPGObject {
                paymentBankCardPGId
                issuedDate
                cardHolder
                cardNumber
              }
              ... on PaymentMoMoPGObject {
                paymentMoMoId
                qrContent
              }
              ... on PaymentZaloPayPGObject {
                paymentZaloPayId
                qrContent
              }
            }
          }
        }
      }
    }
  }
}`;
