import {
  GRAPHQL_DEV,
  GRAPHQL_SANBOX,
  GRAPHQL_STAGGING,
  GRAPHQL_PRODUCTION,
} from '../configs/api.config';

import { callApiRSA } from './middleware/requestRSA';

const getDomain = (env) => {
  switch (env) {
    case 'dev':
      return GRAPHQL_DEV;
    case 'sandbox':
      return GRAPHQL_SANBOX;
    case 'staging':
      return GRAPHQL_STAGGING;
    default:
      return GRAPHQL_PRODUCTION;
  }
};

const getSecure = (env) => {
  switch (env) {
    case 'dev':
      return false;
    case 'sandbox':
      return true;
    case 'staging':
      return true;
    case 'production':
      return true;
    default:
      return true;
  }
};

export async function callGraphql(
  sql,
  variables,
  keys = {
    env: '',
    publicKey: '',
    privateKey: '',
    accessToken: '',
    appId: '',
  }
) {
  return callApiRSA({
    env: keys.env?.toLowerCase(),
    domain: getDomain(keys.env?.toLowerCase()),
    method: 'POST',
    pathUrl: '/graphql',
    accessToken: keys.accessToken ?? '',
    body: {
      query: `${sql}`,
      variables: variables || null,
    },
    isSecurity: getSecure(keys.env?.toLowerCase()),
    publicKey: keys.publicKey,
    privateKey: keys.privateKey,
    xApi: keys?.appId ?? '',
  });
}
