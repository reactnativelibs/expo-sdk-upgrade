import Axios from 'axios';

async function PostUpload(url, accessToken = '', body = {}) {
  const result = {
    code: -2,
    data: {},
    original: null,
  };
  try {
    const response = await Axios.post(url, body, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': accessToken,
      },
      timeout: 30000,
    });

    if (response.code !== 200) {
      result.code = -response.code;
      result.data = {};
      result.original = response.data;
    }
    const { data } = response;
    return {
      code: 1,
      data,
      original: response.data,
    };
  } catch (error) {
    return {
      code: -2,
      data: {
        message: error.message,
      },
      original: error,
    };
  }
}

function handleResponse(response) {
  if (response.code === 1) {
    if (!isNaN(parseInt(response.data))) {
      return {
        code: parseInt(response.data),
        data: {
          message: `Không thể kết nối đến server (Mã lỗi : ${response.data} )`,
        },
      };
    }
    // if (response.data && response.data.code === 401) {
    //   appStore.navigationStack = appStore.navigationStackConstant.AUTH
    //   userStore.logout()
    //   messageInfo.next({ message: response?.data?.message ?? 'Phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại.Xin cảm ơn' })
    // }
    if (response.data?.code === 400) {
      return {
        code: response.data.code,
        data: {
          ...response.data.data,
          message: 'Tham số không hợp lệ',
        },
      };
    }
    return {
      code: response.data.code,
      data: response.data.data,
    };
  }

  if (response.code === -2) {
    if (response.data.message === 'Network Error') {
      return {
        code: -1,
        data: {
          message:
            'Kết nối mạng bị sự cố, vui lòng kiểm tra và thử lại. Xin cảm ơn !',
        },
      };
    }
    if (
      response.data.message === 'timeout of 30000ms exceeded' ||
      response.data.message === 'timeout of 60000ms exceeded'
    ) {
      return {
        code: -3,
        data: {
          message:
            'Kết nối đến máy chủ quá lâu, vui lòng kiểm tra và thử lại. Xin cảm ơn !',
        },
      };
    }
    if (response?.original?.response?.status) {
      return {
        code: response.code,
        data: {
          message: `Không thể kết nối đến server (Mã lỗi : ${response.original.response.status} )`,
        },
      };
    }
  }
  return response;
}

export const callApiUpload = async ({ url, accessToken, body }) => {
  const response = await PostUpload(url, accessToken, body);
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    // console.log("[REQUEST-UPLOAD]", url, body, response)
  }
  return handleResponse(response);
};
