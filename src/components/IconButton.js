import React from 'react';
import { TouchableOpacity } from 'react-native';

export const IconButton = ({ style, icon, onPress = () => null, ...props }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={onPress}
      style={style}
      hitSlop={{
        top: 30,
        left: 30,
        right: 30,
        bottom: 30,
      }}
      {...props}
    >
      {icon}
    </TouchableOpacity>
  );
};
