import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

export const Loading = () => (
  <View style={styles.loading}>
    <ActivityIndicator size="large" color="green" />
  </View>
);

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
