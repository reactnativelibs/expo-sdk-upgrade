/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { Images } from '../images';
import { IconButton } from './IconButton';

export const NotPermissionScreen = ({
  handleClose,
  handleRequestPermission,
}) => {
  return (
    <View
      style={{
        backgroundColor: 'rgb(29, 29, 39)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
      }}
    >
      <IconButton
        icon={
          <Image
            source={Images.iconBackWhite}
            style={{ width: 32, height: 32 }}
          />
        }
        style={{ position: 'absolute', top: 10, left: 10 }}
        onPress={handleClose}
      />

      <Image source={require('../images/noCameraAccess.png')} />
      <Text
        style={{
          fontSize: 18,
          fontWeight: 'bold',
          color: 'white',
          marginVertical: 20,
        }}
      >
        Cho phép truy cập máy ảnh của bạn
      </Text>
      <Text
        style={{
          fontSize: 16,
          color: 'white',
          marginBottom: 10,
          textAlign: 'center',
        }}
      >
        Bạn hãy cho phép Payme truy cập thư máy ảnh trong phần Cài đặt của hệ
        thống để tiếp tục
      </Text>

      <TouchableOpacity
        activeOpacity={0.7}
        onPress={handleRequestPermission}
        style={{
          backgroundColor: '#0CAA26',
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 10,
          paddingHorizontal: 26,
          borderRadius: 16,
        }}
      >
        <Text
          style={{
            fontSize: 16,
            color: 'white',
          }}
        >
          ĐÃ HIỂU
        </Text>
      </TouchableOpacity>
    </View>
  );
};
