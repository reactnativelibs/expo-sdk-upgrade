/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export const Button = ({
  styles = {},
  mainColor = 'red',
  text = '',
  outline = false,
  textStyle = {},
  onPress = () => null,
  ...props
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={[
        s.container,
        {
          borderWidth: outline ? 1 : 0,
          backgroundColor: outline ? 'white' : mainColor,
          borderColor: mainColor,
        },
        styles,
      ]}
      onPress={() => {
        onPress && onPress();
      }}
      {...props}
    >
      <Text
        style={[s.text, { color: outline ? mainColor : 'white' }, textStyle]}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const s = StyleSheet.create({
  container: {
    borderRadius: 20,
    padding: 12,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#ffffff',
  },
});
