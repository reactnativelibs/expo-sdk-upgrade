/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Animated, Dimensions, Easing, View } from 'react-native';

export class ModalView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      animation: new Animated.Value(0),
    };
  }

  componentDidMount() {
    this.setState({ isModalVisible: true }, () => {
      Animated.timing(this.state.animation, {
        toValue: 1,
        duration: 250,
        easing: Easing.linear(),
        useNativeDriver: false,
      }).start();
    });
  }

  close = (callback) => {
    Animated.timing(this.state.animation, {
      toValue: 0,
      duration: 250,
      easing: Easing.linear(),
      useNativeDriver: false,
    }).start(() => {
      this.setState({ isModalVisible: false }, () => {
        if (callback) callback();
      });
    });
  };

  getAnimationStyle = () => {
    switch (this.props.style) {
      case 'left':
        return {
          right: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [Dimensions.get('window').width, 0],
          }),
        };
      case 'right':
        return {
          left: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [Dimensions.get('window').width, 0],
          }),
        };
      case 'top':
        return {
          bottom: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [Dimensions.get('window').height, 0],
          }),
        };
      case 'bottom':
        return {
          top: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [Dimensions.get('window').height, 0],
          }),
        };

      default:
        return {
          top: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [Dimensions.get('window').height, 0],
          }),
        };
    }
  };

  render() {
    const { isModalVisible } = this.state;
    if (!isModalVisible) return null;
    return (
      <View
        style={{
          position: 'absolute',
          backgroundColor: 'transparent',
          width: '100%',
          height: '100%',
        }}
      >
        <Animated.View
          style={[
            {
              flex: 1,
              backgroundColor: 'transparent',
            },
            { ...this.getAnimationStyle() },
            { ...this.props.containerStyle } ?? {},
          ]}
        >
          {this.props.children}
        </Animated.View>
      </View>
    );
  }
}
