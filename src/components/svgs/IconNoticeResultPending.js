import * as React from 'react';
import Svg, { G, Path } from 'react-native-svg';

export function IconNoticePending() {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={112}
      height={112}
      viewBox="0 0 112 112"
    >
      <G fill="none" fillRule="evenodd">
        <G>
          <G>
            <G>
              <G>
                <G
                  fill="#000"
                  fillOpacity={0.2}
                  fillRule="nonzero"
                  opacity={0.15}
                >
                  <Path
                    transform="translate(-132.000000, -60.000000) translate(132.000000, 60.000000) translate(0.437500, 4.375000) translate(4.214882, 17.522598)"
                    d="M56.76 9.714C41.87 6.67 35.916-3.462 18.986 1.213 3.997 5.36-5.14 21.97 3.112 35.872c2.356 3.961 6.36 6.954 8.002 11.4 2.64 7.126-1.378 12.641-.6 19.674 3.262 29.215 47.944 17.892 64.382 8.807 25.781-14.25 43.225-45.967 23.554-61.066-13.668-10.496-26.916-1.952-41.69-4.973z"
                  />
                </G>
                <Path
                  fill="#D8D8D8"
                  d="M7.852 25.082c-1.35.011-2.572-.791-3.097-2.032-.524-1.241-.246-2.675.705-3.632.95-.956 2.385-1.245 3.633-.732 1.248.512 2.062 1.726 2.062 3.073.004 1.826-1.473 3.312-3.303 3.323M3.133 75.782c-1.258.013-2.399-.734-2.888-1.89-.49-1.156-.231-2.493.654-3.384.885-.892 2.223-1.161 3.385-.683 1.163.479 1.921 1.61 1.92 2.865.006 1.7-1.368 3.083-3.071 3.092M91.664 82.47h-1.491v-1.465c.023-.434-.196-.846-.57-1.07-.373-.224-.84-.224-1.213 0-.373.224-.592.636-.569 1.07v1.464h-1.467c-.654 0-1.184.53-1.184 1.182 0 .652.53 1.181 1.184 1.181h1.454v1.499c-.024.434.195.845.568 1.07.374.224.84.224 1.214 0 .374-.225.592-.636.569-1.07v-1.499h1.491c.654 0 1.184-.529 1.184-1.181 0-.653-.53-1.182-1.184-1.182M90.923 1.75h-.836v-.582C90.053.513 89.511 0 88.854 0c-.657 0-1.2.513-1.234 1.168v.582h-.604c-.66 0-1.194.534-1.194 1.192 0 .658.534 1.192 1.194 1.192h.604v.804c.035.654.577 1.168 1.234 1.168s1.199-.514 1.233-1.168V4.12h.836c.66 0 1.195-.533 1.195-1.192 0-.658-.535-1.191-1.195-1.191M25.305 85.277h-.932v-.681c0-.758-.616-1.373-1.375-1.373-.76 0-1.375.615-1.375 1.373v.68h-.683c-.765 0-1.385.62-1.385 1.383 0 .764.62 1.383 1.385 1.383h.683v.936c0 .758.615 1.373 1.375 1.373s1.375-.615 1.375-1.373v-.936h.932c.765 0 1.385-.62 1.385-1.383s-.62-1.382-1.385-1.382"
                  transform="translate(-132.000000, -60.000000) translate(132.000000, 60.000000) translate(0.437500, 4.375000)"
                />
                <Path
                  fill="#FC8C00"
                  transform="translate(-132.000000, -60.000000) translate(132.000000, 60.000000) translate(0.437500, 4.375000)"
                  d="M88.415 49.161c.002 13.87-8.369 26.375-21.21 31.683-12.84 5.31-27.621 2.377-37.45-7.43-9.829-9.807-12.77-24.556-7.451-37.37 5.318-12.814 17.849-21.169 31.748-21.169 18.977 0 34.361 15.35 34.363 34.286"
                />
              </G>
              <Path
                fill="#fff"
                fill-rule="nonzero"
                transform="translate(-132.000000, -60.000000) translate(132.000000, 60.000000) translate(0.437500, 4.375000)"
                d="M52.5 30.625c1.656 0 3.005 1.315 3.06 2.957l.002.105V49.72l13.621 6.164c1.508.683 2.195 2.435 1.57 3.954l-.042.099c-.683 1.508-2.436 2.195-3.954 1.57l-.1-.043-15.42-6.978c-1.06-.48-1.754-1.517-1.797-2.674l-.002-.116V33.688c0-1.692 1.37-3.063 3.062-3.063z"
              />
            </G>
          </G>
        </G>
      </G>
    </Svg>
  );
}
