import * as React from 'react';
import Svg, { G, Path, Circle } from 'react-native-svg';
import { useScaleRatio } from '../../helper/useScaleRatio';

export function ButtonCapture({ color = '#6756D6' }) {
  const scaleRatio = useScaleRatio();
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={80}
      height={80}
      viewBox="0 0 80 80"
    >
      <G fill="none" fillRule="evenodd">
        <Circle cx={40} cy={40} r={37} stroke="#FFF" strokeWidth={6} />
        <Circle cx={40} cy={40} r={32} fill="#FFF" />
        <Path
          fill={color}
          d="M45.365 27a1.34 1.34 0 011.31 1.061l.548 2.604h5.412c1.889 0 3.365 1.204 3.365 2.739v17.462c0 1.538-1.476 2.74-3.365 2.74h-25.27c-1.886 0-3.365-1.202-3.365-2.74V33.404c0-1.535 1.476-2.739 3.365-2.739h5.412l.549-2.604A1.339 1.339 0 0134.636 27zM40 35.574c-3.565 0-6.468 2.9-6.468 6.465A6.475 6.475 0 0040 48.507c3.566 0 6.465-2.9 6.465-6.468 0-3.565-2.9-6.465-6.465-6.465zm0 2.673a3.798 3.798 0 013.791 3.792 3.795 3.795 0 01-3.79 3.79 3.793 3.793 0 01-3.792-3.79c0-2.09 1.7-3.792 3.791-3.792z"
        />
      </G>
    </Svg>
  );
}
