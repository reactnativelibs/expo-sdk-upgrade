import * as React from 'react';
import Svg, { G, Path, Circle } from 'react-native-svg';
import { useScaleRatio } from '../../helper/useScaleRatio';

export function ButtonRecord({ color = '#6756D6' }) {
  const scaleRatio = useScaleRatio();
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={80}
      height={80}
      viewBox="0 0 80 80"
    >
      <G fill="none" fillRule="evenodd">
        <Circle cx={40} cy={40} r={37} stroke="#FFF" strokeWidth={6} />
        <Circle cx={40} cy={40} r={32} fill="#FFF" />
        <Path
          fill={color}
          fillRule="nonzero"
          d="M45.242 29a2.503 2.503 0 012.5 2.5v16.452a2.503 2.503 0 01-2.5 2.5H24.5a2.503 2.503 0 01-2.5-2.5V31.5a2.503 2.503 0 012.5-2.5zM58 31.665V47.84l-8.591-4.69v-6.795L58 31.665z"
        />
      </G>
    </Svg>
  );
}
