export const Animations = {
  kycChupGTTT: require('./assets/animations/ChupGTTT.json'),
  kycQuayVideo: require('./assets/animations/QuayVideo.json'),
  kycDangquay_5: require('./assets/animations/Dangquay_5.json'),
  kycXacthuckhuonmat: require('./assets/animations/Xacthuckhuonmat.json'),
  processingUpload: require('./assets/animations/Proccessing_Upload.json'),
};
