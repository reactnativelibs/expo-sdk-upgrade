import CryptoJS from 'crypto-js';
import { WALLET_ACTIONS } from '../constants';

export class CreateUrlWebPaymeSdk {
  ENV = {
    dev: 'dev',
    sandbox: 'sandbox',
    production: 'production',
  };

  constructor(configs) {
    this.configs = configs;
    this.domain = this.getDomain(configs.env);
  }

  getDomain(env) {
    switch (env) {
      case this.ENV.dev:
        return 'https://dev-sdk.payme.com.vn';

      case this.ENV.sandbox:
        return 'https://sbx-sdk.payme.com.vn';
      case this.ENV.production:
        return 'https://sdk.payme.com.vn';
      default:
        return 'https://sbx-sdk.payme.com.vn';
    }
  }

  encrypt(data) {
    const secretKey = 'CMo359Lqx16QYi3x';
    return CryptoJS.AES.encrypt(JSON.stringify(data), secretKey).toString();
  }

  createLoginURL() {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.LOGIN,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createGetWalletInfoURL() {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.GET_WALLET_INFO,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createGetAccountInfoURL() {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.GET_ACCOUNT_INFO,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createOpenWalletURL() {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.OPEN_WALLET,
      },
    };
    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createOpenHistoryURL() {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.OPEN_HISTORY,
      },
    };
    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createDepositURL(amount, description, extraData, closeWhenDone) {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.DEPOSIT,
        amount,
        description,
        extraData,
        closeWhenDone,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createWithdrawURL(amount, description, extraData, closeWhenDone) {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.WITHDRAW,
        amount,
        description,
        extraData,
        closeWhenDone,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createTransferURL(amount, description, extraData, closeWhenDone) {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.TRANSFER,
        amount,
        description,
        extraData,
        closeWhenDone,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createPayURL(param) {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.PAY,
        amount: param.amount,
        orderId: param.orderId,
        storeId: param.storeId,
        note: param.note,
        userName: param.userName,
        extractData: param.extractData,
        isShowResultUI: param.isShowResultUI,
        method: param.method,
        payCode: param.payCode,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createGetListServiceURL() {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.GET_LIST_SERVICE,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createOpenServiceURL(serviceCode) {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.UTILITY,
        serviceCode,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }

  createGetListPaymentMethodURL(storeId) {
    const configs = {
      ...this.configs,
      actions: {
        type: WALLET_ACTIONS.GET_LIST_PAYMENT_METHOD,
        storeId,
      },
    };

    return (
      this.domain +
      '/getDataWithAction/' +
      encodeURIComponent(this.encrypt(configs))
    );
  }
}
