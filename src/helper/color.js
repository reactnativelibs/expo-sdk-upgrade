export function hexToRgbA(hex, blur = 1) {
  let c;
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split('');
    if (c.length === 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = `0x${c.join('')}`;
    return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(
      ','
    )},${blur})`;
  }
  throw new Error('Bad Hex');
}

export function RgbaToHex(color) {
  const rgba = color.replace(/^rgba?\(|\s+|\)$/g, '').split(',');
  const hex = `#${(
    (1 << 24) +
    (parseInt(rgba[0]) << 16) +
    (parseInt(rgba[1]) << 8) +
    parseInt(rgba[2])
  )
    .toString(16)
    .slice(1)}`;

  return hex;
}

export function opacityColorRgba(color, opacity) {
  return color.replace(/[^,]+(?=\))/, `${opacity}`);
}

export function opacityColorHex(colorHex, opacity) {
  const rgbaColor = hexToRgbA(colorHex);
  const colorOpacity = rgbaColor.replace(/[^,]+(?=\))/, `${opacity}`);
  return colorOpacity;
}
