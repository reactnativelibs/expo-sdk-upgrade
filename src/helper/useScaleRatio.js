import { Dimensions } from 'react-native';
const BASE_WIDTH = 375;
const LIMIT_RATIO = 1.2;

export const useScaleRatio = (factor = 0.5) => {
  const dimensions = Dimensions.get('window');
  const scaleRatio = dimensions.width / BASE_WIDTH;

  if (scaleRatio >= 1 / LIMIT_RATIO && scaleRatio <= LIMIT_RATIO) {
    return scaleRatio;
  }
  return scaleRatio - Math.abs(scaleRatio - 1) * factor;
};
