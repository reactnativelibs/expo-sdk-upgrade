import React from 'react';
// import LottieView from 'lottie-react-native';
import { Dimensions } from 'react-native';
import { Animations } from '../ui';
import { colouriseLottie } from './colouriseLottie';
import { KYC1 } from '../components/svgs/KYC1';
import { KYC2 } from '../components/svgs/KYC2';
import { KYC3 } from '../components/svgs/KYC3';

export const getAnimationKYC = (type, color = '#06d328', styles = {}) => {
  if (type === 'identifyImg') {
    const colorByPath = {
      'layers.8.shapes.0.it.1.c.k': color,
      'layers.8.shapes.1.it.1.c.k': color,
      'layers.8.shapes.13.it.1.c.k': color,
    };
    const src = colouriseLottie(Animations.kycChupGTTT, colorByPath);
    const colorFilters = [
      {
        keypath: 'CMNN_2_xanh',
        color: color,
      },
      {
        keypath: 'CMNN_xanh',
        color: color,
      },
      {
        keypath: 'Focus_xanh',
        color: color,
      },
      {
        keypath: 'Chup_xanh',
        color: color,
      },
      {
        keypath: 'Bo_xanh',
        color: color,
      },
    ];

    return (
      // <LottieView
      //   source={src}
      //   style={{ width: Dimensions.get('window').width / 2, ...styles }}
      //   autoPlay
      //   loop
      //   colorFilters={colorFilters}
      // />
      <KYC1 color={color} />
    );
  }

  if (type === 'kycVideo') {
    const colorFilters = [
      {
        keypath: 'Focus',
        color: color,
      },
      {
        keypath: 'CMNN_bg',
        color: color,
      },
    ];
    return (
      // <LottieView
      //   source={Animations.kycQuayVideo}
      //   style={{ width: Dimensions.get('window').width / 2, ...styles }}
      //   autoPlay
      //   loop
      //   colorFilters={colorFilters}
      // />
      <KYC3 color={color} />
    );
  }

  if (type === 'faceImg') {
    const colorFilters = [
      {
        keypath: 'Focus',
        color: color,
      },
      {
        keypath: 'Mat',
        color: color,
      },
    ];
    return (
      // <LottieView
      //   source={Animations.kycXacthuckhuonmat}
      //   style={{ width: Dimensions.get('window').width / 2, ...styles }}
      //   autoPlay
      //   loop
      //   colorFilters={colorFilters}
      // />
      <KYC2 color={color} />
    );
  }
};
