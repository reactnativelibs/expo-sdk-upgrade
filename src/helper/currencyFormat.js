import numeral from 'numeral';

export const insertCurrency = (str) => {
  let tmp = `${str}` || '';

  if (tmp !== '') {
    tmp = tmp.replace(/[^\d.]+/g, '');
    tmp = numeral(tmp).format('0,0');
  }

  return tmp;
};
