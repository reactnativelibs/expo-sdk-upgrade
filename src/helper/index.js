const prefixPhoneNation = '0';
const prefixPhoneGlobal = '84';

export function convertPhoneNumberNation(number) {
  if (!number) return '';
  let suffix = '';
  if (number.toString().slice(0, 2) === '84') {
    suffix = prefixPhoneNation + number.replace(/^.{2}/g, '');
  } else {
    suffix = number;
  }

  return suffix;
}
