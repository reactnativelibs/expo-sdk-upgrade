/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Alert,
  Modal,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
  AppState,
} from 'react-native';
import { WebView } from 'react-native-webview';
import ScanQR from './scanqr/ScanQR';
import Kyc from './kyc/Kyc';
import { callApiUpload } from './apis/apiUpload';
import {
  accountInit,
  clientRegister,
  detectDataQRCode,
  findAccount,
  getMerchantInfo,
  getSettingServiceMain,
  getTransactionVNPay,
  getWalletInfo,
  payVNPay,
  unlink,
  updateKycAccount,
} from './apis';
import { ModalView } from './components/ModalView';
import { Images } from './images';
import { encryptAES } from './helper/createConnectToken';
import {
  LANGUAGES,
  ENV,
  ERROR_CODE,
  AccountStatus,
  WALLET_ACTIONS,
  PAY_CODE,
  ACCOUNT_STATUS,
} from './constants/index';
import { getTranslate } from './translates';
import { CreateUrlWebPaymeSdk } from './helper/CreateUrlWebPaymeSdk';
import { Loading } from './components/Loading';
import { VNPayResult } from './VNPay/VNPay';
import * as Contacts from 'expo-contacts';
import * as MediaLibrary from 'expo-media-library';
import * as FileSystem from 'expo-file-system';
import * as LinkingExpo from 'expo-linking';
import { Processing } from './VNPay/Processing';
import { getStatusBarHeight } from './helper/iphoneXHelper';

const api_upload = 'static.payme.vn';
const API_UPLOAD_SANBOX = `https://sbx-${api_upload}/Upload`;
const API_UPLOAD = `https://${api_upload}/Upload`;

export class ExpoPaymeSDK extends Component {
  constructor(props) {
    super(props);
    this.state = {
      intervalId: null,
      modalVisible: { state: false, isPay: false },
      uri: '',
      VNPayIsProcessing: false,
      VNPayIsOpenPopup: false,
      VNPayDataResult: {},
      VNPayDataHeader: {},
      isScanQR: false,
      isCallFuncScanQR: false,
      payCodeScanQR: '',
      kyc: {
        isKyc: false,
        kycMode: '',
      },
      appState: AppState.currentState,
      buttonCloseNapas: false,
    };
    this.transactionVNPay = '';
    this.isShowResultUIVNPay = false;
    this.configs = {
      lang: LANGUAGES.VI,
    };
    this._createUrlWebPaymeSdk = null;
    this.isLogin = false;
    this._refWebview = null;
    this._refModalScanQR = null;
    this._refModalVNPay = null;
    this._refModalKyc = null;
    this._timeOut = null;
    this._onSuccess = {};
    this._onError = {};
  }

  componentWillUnmount = () => {
    clearInterval(this.state.intervalId);
    AppState.removeEventListener('change', this._handleAppStateChange);
  };

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      const intervalId = setInterval(this.getResult, 3000);
      // store intervalId in the state so it can be accessed later:
      this.setState({ intervalId: intervalId });
    } else {
      this.state.intervalId && clearInterval(this.state.intervalId);
    }
    this.setState({ appState: nextAppState });
  };

  uploadFile = async (data) => {
    return callApiUpload({
      url: this.configs.env === 'sandbox' ? API_UPLOAD_SANBOX : API_UPLOAD,
      accessToken: this.configs.accessToken ?? '',
      body: data,
    });
  };

  onCloseWebview = (is401 = false) => {
    if (
      this.state.modalVisible.isPay &&
      this._onError[`${WALLET_ACTIONS.PAY}`] &&
      this._onSuccess[`${WALLET_ACTIONS.PAY}`] &&
      !is401
    ) {
      this._onError[`${WALLET_ACTIONS.PAY}`]({
        code: ERROR_CODE.USER_CANCELLED,
        message: 'Đóng modal thanh toán',
      });
      this._onError[`${WALLET_ACTIONS.PAY}`] = null;
    }
    this.setState({
      modalVisible: { state: false, isPay: false },
      uri: '',
      isScanQR: false,
      VNPayIsOpenPopup: false,
      VNPayDataResult: {},
      isCallFuncScanQR: false,
      payCodeScanQR: '',
      kyc: {
        isKyc: false,
        data: {},
      },
      buttonCloseNapas: false,
    });
  };

  closeVNPay = () => {
    if (this._onError[`${WALLET_ACTIONS.PAY}`]) {
      this._onError[`${WALLET_ACTIONS.PAY}`]({
        code: ERROR_CODE.USER_CANCELLED,
        message: 'Cancel/Dismiss VNPay',
      });
      this._onError[`${WALLET_ACTIONS.PAY}`] = null;
    }

    if (this.state.intervalId) {
      clearInterval(this.state.intervalId);
      AppState.removeEventListener('change', this._handleAppStateChange);
    }
    this.setState({
      VNPayIsOpenPopup: false,
      VNPayDataResult: {},
      modalVisible: { state: false },
    });
  };

  getResult = async () => {
    const keys = {
      env: this.configs.env,
      publicKey: this.configs.publicKey,
      privateKey: this.configs.privateKey,
      accessToken: this.configs.accessToken,
      appId: this.configs.appId,
    };

    const responseResult = await getTransactionVNPay(
      { transaction: this.transactionVNPay },
      keys
    );

    if (responseResult.status) {
      if (
        responseResult?.response?.OpenEWallet?.Payment?.GetTransactionInfo
          ?.succeeded
      ) {
        if (
          responseResult?.response?.OpenEWallet?.Payment?.GetTransactionInfo
            ?.state === 'SUCCEEDED'
        ) {
          if (this.state.intervalId) {
            clearInterval(this.state.intervalId);
            AppState.removeEventListener('change', this._handleAppStateChange);
          }
          if (!this.isShowResultUIVNPay) {
            this.onCloseWebview();
          } else {
            this.setState({
              VNPayIsOpenPopup: true,
              VNPayDataResult:
                responseResult?.response?.OpenEWallet?.Payment
                  ?.GetTransactionInfo ?? {},
              modalVisible: { state: true },
            });
          }
          this.sendRespone({
            type: WALLET_ACTIONS.PAY,
            data: {
              transaction:
                responseResult?.response?.OpenEWallet?.Payment
                  ?.GetTransactionInfo?.transaction,
              state:
                responseResult?.response?.OpenEWallet?.Payment
                  ?.GetTransactionInfo?.state,
            },
          });
        }
      } else {
        this.sendRespone({
          type: WALLET_ACTIONS.PAY,
          error: {
            code: ERROR_CODE.SYSTEM,
            message:
              responseResult.response?.Client?.Register?.message ??
              'Có lỗi từ máy chủ hệ thống',
          },
        });
      }
    } else {
      if (responseResult.response[0]?.extensions?.code === ERROR_CODE.EXPIRED) {
        this.sendRespone({
          type: WALLET_ACTIONS.PAY,
          error: {
            code: ERROR_CODE.SYSTEM,
            message:
              responseResult.response[0]?.extensions?.message ??
              'Có lỗi từ máy chủ hệ thống',
          },
        });
      } else {
        this.sendRespone({
          type: WALLET_ACTIONS.PAY,
          error: {
            code: ERROR_CODE.SYSTEM,
            message:
              responseResult?.response?.message ?? 'Có lỗi từ máy chủ hệ thống',
          },
        });
      }
    }
  };

  sendRespone = (data) => {
    const onSuccess = this._onSuccess[data?.type];
    const onError = this._onError[data?.type];

    const sendErrorAny = () => {
      if (onError) {
        onError(data?.error);
        this.clearCallback(data?.type);
      } else {
        for (const [type, callback] of Object.entries(this._onError)) {
          if (callback) {
            callback(data?.error);
            this.clearCallback(type);
          }
        }
      }
    };

    if (data?.error) {
      if (
        data?.error?.code === ERROR_CODE.EXPIRED ||
        data?.error?.code === ERROR_CODE.ACCOUNT_LOCK
      ) {
        this.isLogin = false;
        sendErrorAny();
        return;
      }

      if (data?.error?.code === -2) {
        const error = {
          ...data?.error,
          message:
            data?.error?.message?.message ??
            data?.error?.message ??
            'Network Error!',
        };
        if (onError) onError(error);
        this.clearCallback(data?.type);
        return;
      }

      sendErrorAny();
    } else {
      if (onSuccess) onSuccess(data?.data);
      this.clearCallback(data?.type);
    }
  };

  clearCallback = (type) => {
    this._onSuccess[type] = null;
    this._onError[type] = null;
  };

  saveImage = async (url) => {
    const base64Code = url.split('data:image/png;base64,')[1];

    const filename = FileSystem.documentDirectory + 'VietQR.png';
    await FileSystem.writeAsStringAsync(filename, base64Code, {
      encoding: FileSystem.EncodingType.Base64,
    });

    const permission = await MediaLibrary.requestPermissionsAsync();
    if (!permission.canAskAgain || permission.status === 'denied') {
      /**
       *   Code to open device setting then the user can manually grant the app
       *  that permission
       */

      Linking.openSettings();
    } else {
      if (permission.granted) {
        try {
          const asset = await MediaLibrary.createAssetAsync(filename);
          MediaLibrary.createAlbumAsync('Images', asset, false)
            .then(() => {
              console.log('File Saved Successfully!');
              Alert.alert('Thông báo', 'Tải xuống QR Code thành công');
            })
            .catch(() => {
              console.log('Error In Saving File!');
              Alert.alert('Thông báo', 'Lỗi trong khi tải xuống QR Code');
            });
        } catch (error) {
          console.log(error);
          Alert.alert('Thông báo', error);
        }
      } else {
        console.log('Need Storage permission to save file');
        Alert.alert(
          'Thông báo',
          'Tải xuống QR Code thất bại (Permission not granted)'
        );
      }
    }
  };

  handleGetContacts = async () => {
    const runPermissionDeny = () => {
      const run = `
          window.onPermission(false);
          true; // note: this is required, or you'll sometimes get silent failures
        `;
      this._refWebview?.injectJavaScript(run);
    };
    const runPermissionGranted = async () => {
      const { data } = await Contacts.getContactsAsync({
        fields: [Contacts.Fields.PhoneNumbers],
      });

      const list = data.reduce(function (result, item) {
        const number = item.phoneNumbers?.[0]?.number;
        if (number) {
          result.push({
            name: item.name,
            phone: number,
          });
        }
        return result;
      }, []);
      if (list.length > 0) {
        const run = `
          window.onContacts(\`${JSON.stringify(list)}\`);
          window.onPermission(true);
          true; // note: this is required, or you'll sometimes get silent failures
        `;
        this._refWebview?.injectJavaScript(run);
      } else {
        const run = `
          window.onContacts(\`${JSON.stringify([])}\`);
          window.onPermission(true);
          true; // note: this is required, or you'll sometimes get silent failures
        `;
        this._refWebview?.injectJavaScript(run);
      }
    };

    const { granted } = await Contacts.getPermissionsAsync();

    if (!granted) {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        runPermissionGranted();
      } else {
        runPermissionDeny();
      }
    } else {
      runPermissionGranted();
    }
  };

  handleOnOpenSetting = async () => {
    const { status } = await Contacts.requestPermissionsAsync();
    if (status === 'granted') {
      this.handleGetContacts();
    } else {
      Linking.openSettings();
    }
  };

  handleOnMessage = (event) => {
    const data = JSON.parse(event.nativeEvent.data);

    if (data?.type === 'getContacts') {
      this.handleGetContacts();
    }
    if (data?.type === 'onOpenSetting') {
      this.handleOnOpenSetting();
    }
    if (data?.type === 'onClose') {
      this.onCloseWebview();
    }
    if (data?.type === 'onDownload') {
      // this.onCloseWebview();
      this.saveImage(data?.data?.url);
    }
    if (data?.type === WALLET_ACTIONS.GET_WALLET_INFO) {
      this.onCloseWebview();
      this.sendRespone(data);
    }
    if (data?.type === WALLET_ACTIONS.GET_ACCOUNT_INFO) {
      this.onCloseWebview();
      this.sendRespone(data);
    }
    if (data?.type === WALLET_ACTIONS.GET_LIST_SERVICE) {
      this.onCloseWebview();
      this.sendRespone(data);
    }
    if (data?.type === WALLET_ACTIONS.GET_LIST_PAYMENT_METHOD) {
      this.onCloseWebview();
      this.sendRespone(data);
    }
    if (data?.type === WALLET_ACTIONS.LOGIN) {
      this.onCloseWebview();
      if (data?.data) {
        const newConfigs = {
          ...this.configs,
          ...data.data,
        };
        this.configs = newConfigs;
        this._createUrlWebPaymeSdk = new CreateUrlWebPaymeSdk(newConfigs);
        this.isLogin = true;
        const res = {
          ...data,
          data: { accountStatus: data?.data?.accountStatus },
        };
        this.sendRespone(res);
      }
    }
    if (data?.type === WALLET_ACTIONS.RELOGIN) {
      if (data?.data) {
        const newConfigs = {
          ...this.configs,
          ...data.data,
          dataInit: {
            ...(this.configs?.dataInit ?? {}),
            ...data.data,
          },
        };
        this.configs = newConfigs;
        this._createUrlWebPaymeSdk = new CreateUrlWebPaymeSdk(newConfigs);
        this.isLogin = true;
      }
    }

    if (data?.type === 'onScan') {
      this.setState({ isScanQR: true, isCallFuncScanQR: false });
    }
    if (data?.type === WALLET_ACTIONS.PAY) {
      this.sendRespone(data);
    }
    if (data?.type === 'onKyc') {
      this.setState({
        kyc: { isKyc: true, kycMode: data?.kycMode },
      });
    }
    if (data?.type === 'error') {
      if (data?.code === ERROR_CODE.EXPIRED) {
        this.onCloseWebview(true);
        const res = {
          ...data,
          error: { code: ERROR_CODE.EXPIRED, message: data?.message },
        };
        this.sendRespone(res);
      }
      if (data?.code === ERROR_CODE.ACCOUNT_LOCK) {
        this.onCloseWebview(true);
        const res = {
          ...data,
          error: { code: ERROR_CODE.ACCOUNT_LOCK, message: data?.message },
        };
        this.sendRespone(res);
      }
    }
    if (data?.type === WALLET_ACTIONS.PAY_CHECK_ACCOUNT) {
      this.onCloseWebview();
      if (this._timeOut) {
        clearTimeout(this._timeOut);
      }
      this._timeOut = setTimeout(() => {
        if (data?.action === 'DEPOSIT') {
          this.deposit({ amount: 10000 });
        } else {
          this.openWallet();
        }
      }, 100);
    }
    if (
      data?.type === 'onDeposit' ||
      data?.type === 'onWithDraw' ||
      data?.type === 'onTransfer'
    ) {
      this.onCloseWebview();
      const res = { ...data };
      if (data?.data?.status === 'FAILED') {
        res.error = data?.data;
      }
      if (data?.type === 'onDeposit') res.type = WALLET_ACTIONS.DEPOSIT;
      if (data?.type === 'onWithDraw') res.type = WALLET_ACTIONS.WITHDRAW;
      if (data?.type === 'onTransfer') res.type = WALLET_ACTIONS.TRANSFER;
      this.sendRespone(res);
    }
    if (data?.type === 'showButtonCloseNapas') {
      this.setState({
        buttonCloseNapas: data?.isShowButtonClose ?? false,
      });
    }
  };

  _checkActiveAndKyc = () => {
    if (this.configs?.accountStatus !== ACCOUNT_STATUS.KYC_APPROVED) {
      return false;
    }
    return true;
  };

  login = async (configs, onSuccess, onError) => {
    const _configs = {
      ...configs,
      partner: { ...configs?.partner, type: 'expo' },
    };
    this.configs = _configs;
    try {
      const keys = {
        env: configs.env,
        publicKey: configs.publicKey,
        privateKey: configs.privateKey,
        accessToken: '',
        appId: configs.appId,
      };
      const responseClientRegister = await clientRegister(
        {
          deviceId: configs.clientId,
        },
        keys
      );
      if (responseClientRegister.status) {
        if (responseClientRegister.response?.Client?.Register?.succeeded) {
          const responseAccountInit = await accountInit(
            {
              appToken: configs.appToken,
              connectToken: configs.connectToken,
              clientId:
                responseClientRegister.response?.Client?.Register?.clientId,
            },
            keys
          );

          if (responseAccountInit.status) {
            const {
              handShake,
              accessToken = '',
              phone = '',
              storeName = '',
              storeImage = '',
              updateToken,
              kyc,
            } = responseAccountInit.response?.OpenEWallet?.Init ?? {};
            let accountStatus = ACCOUNT_STATUS.NOT_ACTIVATED;

            if (responseAccountInit.response?.OpenEWallet?.Init?.succeeded) {
              if (
                responseAccountInit.response?.OpenEWallet?.Init?.kyc &&
                responseAccountInit.response?.OpenEWallet?.Init?.kyc?.kycId
              ) {
                if (
                  responseAccountInit.response?.OpenEWallet?.Init?.kyc
                    ?.state === 'APPROVED'
                ) {
                  accountStatus = ACCOUNT_STATUS.KYC_APPROVED;
                } else if (
                  responseAccountInit.response?.OpenEWallet?.Init?.kyc
                    ?.state === 'PENDING'
                ) {
                  accountStatus = ACCOUNT_STATUS.KYC_REVIEW;
                } else {
                  accountStatus = ACCOUNT_STATUS.KYC_REJECTED;
                }
              } else {
                accountStatus = ACCOUNT_STATUS.NOT_KYC;
              }
              const responseLogin = {
                data: {
                  accountStatus,
                  handShake,
                  accessToken,
                  storeName,
                  storeImage,
                  phone: configs.phone ? configs.phone : phone,
                  kyc,
                },
              };
              const newConfigs = {
                ...this.configs,
                ...responseLogin.data,
              };
              this.configs = newConfigs;
              this._createUrlWebPaymeSdk = new CreateUrlWebPaymeSdk(newConfigs);
              this.isLogin = true;
              onSuccess({ accountStatus: responseLogin.data.accountStatus });
            } else if (!accessToken && updateToken) {
              const responseLogin = {
                data: {
                  accountStatus,
                  handShake,
                  accessToken,
                  storeName,
                  storeImage,
                  phone: configs.phone ? configs.phone : phone,
                  kyc,
                },
              };
              const newConfigs = {
                ...this.configs,
                ...responseLogin.data,
                dataInit: responseAccountInit.response?.OpenEWallet?.Init ?? {},
              };
              this.configs = newConfigs;
              this._createUrlWebPaymeSdk = new CreateUrlWebPaymeSdk(newConfigs);
              this.isLogin = true;
              onSuccess({ accountStatus: responseLogin.data.accountStatus });
            } else {
              onError({
                code: ERROR_CODE.SYSTEM,
                message:
                  responseAccountInit.response?.OpenEWallet?.Init?.message ??
                  'Có lỗi từ máy chủ hệ thống',
              });
            }
          } else {
            onError({
              code: ERROR_CODE.SYSTEM,
              message:
                responseAccountInit.response[0]?.message ??
                responseAccountInit.response.message ??
                'Có lỗi từ máy chủ hệ thống',
            });
          }
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseClientRegister.response?.Client?.Register?.message ??
              'Có lỗi từ máy chủ hệ thống',
          });
        }
      } else {
        onError({
          code: ERROR_CODE.SYSTEM,
          message:
            responseClientRegister.response[0]?.message ??
            responseClientRegister.response.message ??
            'Có lỗi từ máy chủ hệ thống',
        });
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? 'Có lỗi xảy ra',
      });
    }
  };

  unlinkAccount = async (params, onSuccess, onError) => {
    try {
      const keys = {
        env: params.env,
        publicKey: params.publicKey,
        privateKey: params.privateKey,
        appId: params.appId,
      };
      const responseUnlinkAccount = await unlink(
        {
          appToken: params.appToken,
          connectToken: params.connectToken,
        },
        keys
      );
      if (responseUnlinkAccount.status) {
        if (responseUnlinkAccount.response?.OpenEWallet?.Unlink?.succeeded) {
          onSuccess({
            message:
              responseUnlinkAccount.response?.OpenEWallet?.Unlink?.message,
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseUnlinkAccount.response?.OpenEWallet?.Unlink?.message ||
              'Có lỗi từ máy chủ hệ thống',
          });
        }
      } else {
        if (
          responseUnlinkAccount.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          onError({
            code: ERROR_CODE.EXPIRED,
            message:
              responseUnlinkAccount.response[0]?.extensions?.message ??
              'Thông tin xác thực không hợp lệ',
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseUnlinkAccount.response[0]?.message ??
              responseUnlinkAccount.response.message ??
              'Có lỗi từ máy chủ hệ thống',
          });
        }
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? 'Có lỗi xảy ra',
      });
    }
  };

  openWallet = (onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }
    const uri = this._createUrlWebPaymeSdk.createOpenWalletURL();
    this.setState({
      uri,
      modalVisible: { state: true, isPay: false },
    });
    this._onSuccess[`${WALLET_ACTIONS.OPEN_WALLET}`] = onSuccess;
    this._onError[`${WALLET_ACTIONS.OPEN_WALLET}`] = onError;
  };

  openHistory = (onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }

    if (!this._checkActiveAndKyc()) {
      onError({
        code: ERROR_CODE.KYC_NOT_APPROVED,
        message: this.configs.accountStatus,
      });
      return;
    }

    const uri = this._createUrlWebPaymeSdk.createOpenHistoryURL();
    console.log(uri);
    this.setState({
      uri,
      modalVisible: { state: true, isPay: false },
    });
    this._onSuccess[`${WALLET_ACTIONS.OPEN_HISTORY}`] = onSuccess;
    this._onError[`${WALLET_ACTIONS.OPEN_HISTORY}`] = onError;
  };

  getWalletInfo = async (onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }

    if (!this._checkActiveAndKyc()) {
      onError({
        code: ERROR_CODE.KYC_NOT_APPROVED,
        message: this.configs.accountStatus,
      });
      return;
    }

    try {
      const keys = {
        env: this.configs.env,
        publicKey: this.configs.publicKey,
        privateKey: this.configs.privateKey,
        accessToken: this.configs.accessToken,
        appId: this.configs.appId,
      };
      const responseGetWalletInfo = await getWalletInfo({}, keys);
      if (responseGetWalletInfo.status) {
        onSuccess(responseGetWalletInfo.response?.Wallet ?? {});
      } else {
        if (
          responseGetWalletInfo.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          onError({
            code: ERROR_CODE.EXPIRED,
            message:
              responseGetWalletInfo.response[0]?.extensions?.message ??
              'Thông tin  xác thực không hợp lệ',
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseGetWalletInfo.response?.message ??
              responseGetWalletInfo.response?.[0]?.message ??
              'Có lỗi từ máy chủ hệ thống',
          });
        }
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? 'Có lỗi xảy ra',
      });
    }
  };

  getAccountInfo = async (onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }
    if (!this._checkActiveAndKyc()) {
      onError({
        code: ERROR_CODE.KYC_NOT_APPROVED,
        message: this.configs.accountStatus,
      });
      return;
    }

    try {
      const params = {
        phone: this.configs.phone,
      };
      const keys = {
        env: this.configs.env,
        publicKey: this.configs.publicKey,
        privateKey: this.configs.privateKey,
        accessToken: this.configs.accessToken,
        appId: this.configs.appId,
      };
      const responseFindAccount = await findAccount(params, keys);
      if (responseFindAccount.status) {
        onSuccess(responseFindAccount.response?.Account ?? {});
      } else {
        if (
          responseFindAccount.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          onError({
            code: ERROR_CODE.EXPIRED,
            message:
              responseFindAccount.response[0]?.extensions?.message ??
              'Thông tin  xác thực không hợp lệ',
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseFindAccount.response?.message ??
              responseFindAccount.response?.[0]?.message ??
              'Có lỗi từ máy chủ hệ thống',
          });
        }
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? 'Có lỗi xảy ra',
      });
    }
  };

  deposit = (data, onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }
    if (!this._checkActiveAndKyc()) {
      onError({
        code: ERROR_CODE.KYC_NOT_APPROVED,
        message: this.configs.accountStatus,
      });
      return;
    }
    const uri = this._createUrlWebPaymeSdk.createDepositURL(
      data?.amount,
      data?.description,
      data?.extraData,
      data?.closeWhenDone
    );
    this.setState({
      uri: uri,
      modalVisible: { state: true, isPay: false },
    });
    this._onSuccess[`${WALLET_ACTIONS.DEPOSIT}`] = onSuccess;
    this._onError[`${WALLET_ACTIONS.DEPOSIT}`] = onError;
  };

  withdraw = (data, onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }
    if (!this._checkActiveAndKyc()) {
      onError({
        code: ERROR_CODE.KYC_NOT_APPROVED,
        message: this.configs.accountStatus,
      });
      return;
    }
    const uri = this._createUrlWebPaymeSdk.createWithdrawURL(
      data?.amount,
      data?.description,
      data?.extraData,
      data?.closeWhenDone
    );
    this.setState({
      uri: uri,
      modalVisible: { state: true, isPay: false },
    });
    this._onSuccess[`${WALLET_ACTIONS.WITHDRAW}`] = onSuccess;
    this._onError[`${WALLET_ACTIONS.WITHDRAW}`] = onError;
  };

  transfer = (data, onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }
    if (!this._checkActiveAndKyc()) {
      onError({
        code: ERROR_CODE.KYC_NOT_APPROVED,
        message: this.configs.accountStatus,
      });
      return;
    }
    const uri = this._createUrlWebPaymeSdk.createTransferURL(
      data?.amount,
      data?.description,
      data?.extraData,
      data?.closeWhenDone
    );
    this.setState({
      uri: uri,
      modalVisible: { state: true, isPay: false },
    });
    this._onSuccess[`${WALLET_ACTIONS.TRANSFER}`] = onSuccess;
    this._onError[`${WALLET_ACTIONS.TRANSFER}`] = onError;
  };

  getBalanceInteral = () => {
    return new Promise((resolve) => {
      this.getWalletInfo(
        (res) => {
          resolve({ status: true, data: res });
        },
        (err) => {
          resolve({ status: false, error: err });
        }
      );
    });
  };

  checkPaycode = async (
    params = {
      payCode: '',
      storeId: '',
    },
    onError = () => null
  ) => {
    if (!params?.payCode) {
      onError({
        code: ERROR_CODE.UNKNOWN_PAYCODE,
        message: 'Thiếu thông tin payCode!',
      });
      return false;
    } else if (
      params?.payCode &&
      !Object.values(PAY_CODE).includes(params?.payCode)
    ) {
      onError({
        code: ERROR_CODE.UNKNOWN_PAYCODE,
        message: 'Giá trị payCode không hợp lệ!',
      });
      return false;
    } else if (
      params?.payCode === PAY_CODE.MOMO ||
      params?.payCode === PAY_CODE.ZALO_PAY
    ) {
      onError({
        code: ERROR_CODE.UNKNOWN_PAYCODE,
        message:
          'Phương thức thanh toán không được hỗ trợ. Vui lòng kiểm tra lại!',
      });
      return false;
    }
    return true;
  };

  pay = async (params, onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }

    const keys = {
      env: this.configs.env,
      publicKey: this.configs.publicKey,
      privateKey: this.configs.privateKey,
      accessToken: this.configs.accessToken,
      appId: this.configs.appId,
    };

    //check payCode
    const checkPaycode = await this.checkPaycode(
      {
        payCode: params?.payCode,
      },
      onError
    );

    if (!checkPaycode) {
      return;
    }

    this._onSuccess[`${WALLET_ACTIONS.PAY}`] = onSuccess;
    this._onError[`${WALLET_ACTIONS.PAY}`] = onError;

    if (params?.payCode === PAY_CODE.VN_PAY) {
      try {
        this.setState({ VNPayIsProcessing: true });

        const responseGetMerchantInfo = await getMerchantInfo(
          {
            storeId: params?.storeId,
            appId: this.configs.appId,
          },
          keys
        );

        if (responseGetMerchantInfo.status) {
          if (
            responseGetMerchantInfo?.response?.OpenEWallet?.GetInfoMerchant
              ?.succeeded
          ) {
            this.setState({
              VNPayDataHeader: {
                amount: params?.amount,
                note: params.note,
                orderId: params?.orderId,
                storeName:
                  responseGetMerchantInfo?.response?.OpenEWallet
                    ?.GetInfoMerchant?.storeName,
                storeImage:
                  responseGetMerchantInfo?.response?.OpenEWallet
                    ?.GetInfoMerchant?.storeImage,
                isVisibleHeader:
                  responseGetMerchantInfo?.response?.OpenEWallet
                    ?.GetInfoMerchant?.isVisibleHeader,
              },
            });
            const responsePay = await payVNPay(
              {
                clientId: this.configs.clientId ?? '',
                amount: params?.amount,
                storeId: params?.storeId,
                orderId: params?.orderId,
                note: params.note,
              },
              keys
            );
            this.setState({ VNPayIsProcessing: false });

            if (responsePay.status) {
              if (
                responsePay?.response?.OpenEWallet?.Payment?.Pay?.payment
                  ?.bankQRCodeState === 'REQUIRED_TRANSFER'
              ) {
                this.transactionVNPay =
                  responsePay?.response?.OpenEWallet?.Payment?.Pay?.history?.service?.transaction;
                this.isShowResultUIVNPay = params?.isShowResultUI;
                const linkCanOpen = await LinkingExpo.canOpenURL(
                  responsePay?.response?.OpenEWallet?.Payment?.Pay?.payment
                    ?.qrContent
                );

                if (linkCanOpen) {
                  this.setState({
                    VNPayIsOpenPopup: true,
                    modalVisible: { state: true },
                  });
                  AppState.addEventListener(
                    'change',
                    this._handleAppStateChange
                  );
                  await LinkingExpo.openURL(
                    responsePay?.response?.OpenEWallet?.Payment?.Pay?.payment
                      ?.qrContent
                  );
                } else {
                  onError({
                    code: ERROR_CODE.PAYMENT_ERROR,
                    message: 'Có lỗi xảy ra!',
                  });
                }
              } else {
                onError({
                  code: ERROR_CODE.EXPIRED,
                  message:
                    responsePay?.response?.OpenEWallet?.Payment?.Pay?.message ??
                    'Thông tin xác thực không hợp lệ',
                });
              }
            } else {
              if (
                responsePay.response[0]?.extensions?.code === ERROR_CODE.EXPIRED
              ) {
                onError({
                  code: ERROR_CODE.EXPIRED,
                  message:
                    responsePay.response[0]?.extensions?.message ??
                    'Thông tin xác thực không hợp lệ',
                });
              } else {
                onError({
                  code: ERROR_CODE.SYSTEM,
                  message:
                    responsePay?.response?.message ??
                    'Có lỗi từ máy chủ hệ thống',
                });
              }
              return;
            }
          } else {
            this.setState({ VNPayIsProcessing: false });
            onError({
              code: ERROR_CODE.SYSTEM,
              message:
                responseGetMerchantInfo?.response?.OpenEWallet?.GetInfoMerchant
                  ?.message ?? 'Có lỗi từ máy chủ hệ thống',
            });
            return;
          }
        } else {
          this.setState({ VNPayIsProcessing: false });

          if (
            responseGetMerchantInfo.response[0]?.extensions?.code ===
            ERROR_CODE.EXPIRED
          ) {
            onError({
              code: ERROR_CODE.EXPIRED,
              message:
                responseGetMerchantInfo.response[0]?.extensions?.message ??
                'Thông tin xác thực không hợp lệ',
            });
          } else {
            onError({
              code: ERROR_CODE.SYSTEM,
              message:
                responseGetMerchantInfo?.response?.message ??
                'Có lỗi từ máy chủ hệ thống',
            });
          }
          return;
        }
      } catch (error) {
        this.setState({ VNPayIsProcessing: false });
        onError({
          code: ERROR_CODE.SYSTEM,
          message: error.message ?? 'Có lỗi xảy ra',
        });
        return;
      }
      return;
    }

    //getMerchantInfo
    try {
      const responseGetMerchantInfo = await getMerchantInfo(
        {
          storeId: params?.storeId,
          appId: this.configs.appId,
        },
        keys
      );

      if (responseGetMerchantInfo.status) {
        if (
          responseGetMerchantInfo?.response?.OpenEWallet?.GetInfoMerchant
            ?.succeeded
        ) {
          const newConfigs = {
            ...this.configs,
            storeName:
              responseGetMerchantInfo?.response?.OpenEWallet?.GetInfoMerchant
                ?.storeName,
            storeImage:
              responseGetMerchantInfo?.response?.OpenEWallet?.GetInfoMerchant
                ?.storeImage,
            isVisibleHeader:
              responseGetMerchantInfo?.response?.OpenEWallet?.GetInfoMerchant
                ?.isVisibleHeader,
          };
          this.configs = newConfigs;
          this._createUrlWebPaymeSdk = new CreateUrlWebPaymeSdk(newConfigs);
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseGetMerchantInfo?.response?.OpenEWallet?.GetInfoMerchant
                ?.message ?? 'Có lỗi từ máy chủ hệ thống',
          });
          return;
        }
      } else {
        if (
          responseGetMerchantInfo.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          onError({
            code: ERROR_CODE.EXPIRED,
            message:
              responseGetMerchantInfo.response[0]?.extensions?.message ??
              'Thông tin xác thực không hợp lệ',
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseGetMerchantInfo?.response?.message ??
              'Có lỗi từ máy chủ hệ thống',
          });
        }
        return;
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? 'Có lỗi xảy ra',
      });
      return;
    }

    /*
    if (data.method?.type === METHOD_TYPE.WALLET) {
      if (!this._checkActiveAndKyc()) {
        const error = {
          code: ERROR_CODE.ACCOUNT_NOT_KYC,
          message: "ACCOUNT_NOT_KYC",
        };
        if (this.configs?.accountStatus === AccountStatus.NOT_ACTIVED) {
          error.code = ERROR_CODE.ACCOUNT_NOT_ACTIVATED;
          error.message = "ACCOUNT_NOT_ACTIVATED";
        }
        onError?.(error);
        return;
      } else {
        const res = await this.getBalanceInteral();
        if (res?.status) {
          if (res?.data?.balance < data.amount) {
            onError({
              code: ERROR_CODE.BALANCE_ERROR,
              message: "Số dư ví PayME không đủ",
            });
            return;
          }
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message: res?.error?.message ?? "Có lỗi xảy ra",
          });
          return;
        }
      }
    }*/

    const uri = this._createUrlWebPaymeSdk.createPayURL({
      amount: params.amount,
      orderId: params.orderId,
      storeId: params.storeId,
      userName: params.userName,
      note: params.note,
      extractData: params.extractData,
      isShowResultUI: params.isShowResultUI,
      method: params.method, //-> remove
      payCode: params.payCode,
    });
    this.setState({
      uri: uri,
      modalVisible: { state: true, isPay: true },
    });
  };

  getListService = async (onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }

    try {
      const params = {
        appId: this.configs.appId,
      };
      const keys = {
        env: this.configs.env,
        publicKey: this.configs.publicKey,
        privateKey: this.configs.privateKey,
        accessToken: this.configs.accessToken,
        appId: this.configs.appId,
      };
      const responseGsetSettingServiceMain = await getSettingServiceMain(
        params,
        keys
      );
      if (responseGsetSettingServiceMain.status) {
        if (
          responseGsetSettingServiceMain.response?.Setting?.configs.length > 0
        ) {
          const service =
            responseGsetSettingServiceMain.response?.Setting?.configs?.filter(
              (itemSetting) => itemSetting?.key === 'service.main.visible'
            );
          const valueStr = service[0]?.value ?? '';
          const list = JSON.parse(valueStr)?.listService;
          onSuccess(list);
        } else {
          onSuccess([]);
        }
      } else {
        if (
          responseGsetSettingServiceMain.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          onError({
            code: ERROR_CODE.EXPIRED,
            message:
              responseGsetSettingServiceMain.response[0]?.extensions?.message ??
              'Thông tin  xác thực không hợp lệ',
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseGsetSettingServiceMain.response?.message ??
              responseGsetSettingServiceMain.response?.[0]?.message ??
              'Có lỗi từ máy chủ hệ thống',
          });
        }
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? 'Có lỗi xảy ra',
      });
    }
  };

  openService = (serviceCode, onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }
    if (!this._checkActiveAndKyc()) {
      onError({
        code: ERROR_CODE.KYC_NOT_APPROVED,
        message: this.configs.accountStatus,
      });
      return;
    }

    const uri = this._createUrlWebPaymeSdk.createOpenServiceURL(serviceCode);
    this.setState({
      uri: uri,
      modalVisible: { state: true, isPay: false },
    });
    this._onSuccess[`${WALLET_ACTIONS.OPEN_SERVICE}`] = onSuccess;
    this._onError[`${WALLET_ACTIONS.OPEN_SERVICE}`] = onError;
  };

  /*
  getListPaymentMethod = async (storeId, onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: "NOT_LOGIN" });
      return;
    }

    try {
      const params = {
        storeId,
      };
      const keys = {
        env: this.configs.env,
        publicKey: this.configs.publicKey,
        privateKey: this.configs.privateKey,
        accessToken: this.configs.accessToken,
        appId: this.configs.appId,
      };
      const responseGetPaymentMethod = await getPaymentMethod(params, keys);
      if (responseGetPaymentMethod.status) {
        if (responseGetPaymentMethod.response?.Utility?.GetPaymentMethod?.succeeded) {
          onSuccess(responseGetPaymentMethod.response?.Utility?.GetPaymentMethod?.methods ?? []);
        } else {
          onError({
            code: ERROR_CODE.EXPIRED,
            message:
              responseGetPaymentMethod.response?.Utility?.GetPaymentMethod?.message ?? "Có lỗi từ máy chủ hệ thống",
          });
        }
      } else {
        if (responseGetPaymentMethod.response[0]?.extensions?.code === 401) {
          onError({
            code: ERROR_CODE.EXPIRED,
            message: responseGetPaymentMethod.response[0]?.extensions?.message ?? "Thông tin  xác thực không hợp lệ",
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message: "Có lỗi từ máy chủ hệ thống",
          });
        }
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? "Có lỗi xảy ra",
      });
    }
  };
  */

  scanQR = async (params, onSuccess, onError) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }

    //check payCode
    const checkPaycode = await this.checkPaycode(
      {
        payCode: params?.payCode,
      },
      onError
    );

    if (!checkPaycode) {
      return;
    }

    this.setState({
      modalVisible: { state: true, isPay: false },
      isScanQR: true,
      isCallFuncScanQR: true,
      payCodeScanQR: params?.payCode,
    });
    onSuccess({});
  };

  payQRCode = async (
    params = {
      qr: '',
      isShowResultUI: true,
      payCode: '',
    },
    onSuccess,
    onError
  ) => {
    if (!this.isLogin) {
      onError?.({ code: ERROR_CODE.NOT_LOGIN, message: 'NOT_LOGIN' });
      return;
    }

    const checkPaycode = await this.checkPaycode(
      {
        payCode: params?.payCode,
      },
      onError
    );

    if (!checkPaycode) {
      return;
    }

    try {
      const qr = params.qr;
      const isShowResultUI = params.isShowResultUI;

      const responseDetectQrCode = await this.detectQRCode(qr);
      if (responseDetectQrCode.status) {
        if (
          responseDetectQrCode.response?.OpenEWallet?.Payment?.Detect?.succeeded
        ) {
          const data =
            responseDetectQrCode.response?.OpenEWallet?.Payment?.Detect ?? {};
          this.pay(
            {
              amount: data?.amount,
              orderId: data?.orderId,
              storeId: data?.storeId,
              note: data?.note,
              userName: data?.userName,
              isShowResultUI,
              payCode: params.payCode,
            },
            onSuccess,
            onError
          );
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseDetectQrCode.response?.OpenEWallet?.Payment?.Detect
                ?.message ?? 'Có lỗi từ máy chủ hệ thống',
          });
        }
      } else {
        if (
          responseDetectQrCode.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          onError({
            code: ERROR_CODE.EXPIRED,
            message:
              responseDetectQrCode.response[0]?.extensions?.message ??
              'Thông tin  xác thực không hợp lệ',
          });
        } else {
          onError({
            code: ERROR_CODE.SYSTEM,
            message:
              responseDetectQrCode.response?.message ??
              responseDetectQrCode.response?.[0]?.message ??
              'Có lỗi từ máy chủ hệ thống',
          });
        }
      }
    } catch (error) {
      onError({
        code: ERROR_CODE.SYSTEM,
        message: error.message ?? 'Có lỗi xảy ra',
      });
    }
  };

  onCloseVNPay = () => {
    this._refModalVNPay?.close(() => {
      this.setState({
        modalVisible: { state: false },
        VNPay: { isOpenPopup: false },
      });
    });
  };

  onCloseScanQR = () => {
    if (this.state.isCallFuncScanQR) {
      this.onCloseWebview();
    } else {
      this._refModalScanQR?.close(() => {
        this.setState({ isScanQR: false });
      });
    }
  };

  onCloseKyc = () => {
    this._refModalKyc?.close(() => {
      this.setState({ kyc: { isKyc: false } });
    });
  };

  onSendDataScan = (data) => {
    const run = `
          window.expoSendDataScan(\`${data ?? ''}\`);
          true; // note: this is required, or you'll sometimes get silent failures
        `;
    this._refWebview?.injectJavaScript(run);
    this.onCloseScanQR();
  };

  detectQRCode = async (qr) => {
    const params = {
      input: {
        clientId: this.configs.clientId ?? '',
        qrContent: qr,
      },
    };
    const keys = {
      env: this.configs.env,
      publicKey: this.configs.publicKey,
      privateKey: this.configs.privateKey,
      accessToken: this.configs.accessToken,
      appId: this.configs.appId,
    };
    return await detectDataQRCode(params, keys);
  };

  handlePayScanQr = (data) => {
    this._refModalScanQR?.close(() => {
      this.setState({ isScanQR: false });
      this.pay({
        amount: data?.amount,
        orderId: data?.orderId,
        storeId: data?.storeId,
        note: data?.note,
        payCode: this.state.payCodeScanQR,
      });
    });
  };

  handleErrorWebview = (syntheticEvent) => {
    this.onCloseWebview();
    const res = {
      error: { code: ERROR_CODE.NETWORK, message: 'Lỗi kết nối mạng!' },
    };
    this.sendRespone(res);
  };

  handleUpdateCCCDAccount = async (params) => {
    try {
      const kycInput = {
        clientId: this.configs.clientId ?? '',
        ...params,
      };
      const keys = {
        env: this.configs.env,
        publicKey: this.configs.publicKey,
        privateKey: this.configs.privateKey,
        accessToken: this.configs.accessToken,
        appId: this.configs.appId,
      };
      const responseUpdateKycAccount = await updateKycAccount(kycInput, keys);
      if (responseUpdateKycAccount.status) {
        //success -> reload
        const run = `
          window.onUpdateIdentify();
          true; // note: this is required, or you'll sometimes get silent failures
        `;
        this._refWebview?.injectJavaScript(run);
      } else {
        if (
          responseUpdateKycAccount.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          Alert.alert(
            'Thông báo',
            responseUpdateKycAccount.response[0]?.extensions?.message ??
              'Thông tin xác thực không hợp lệ'
          );
        } else {
          Alert.alert('Thông báo', 'Có lỗi từ máy chủ hệ thống');
        }
      }
    } catch (error) {
      Alert.alert('Thông báo', 'Update CCCD Fail');
    } finally {
      // home, reload get kyc
      this.onCloseKyc();
    }
  };

  handleUpdateKycAccount = async (params) => {
    try {
      const kycInput = {
        clientId: this.configs.clientId ?? '',
        ...params,
      };
      const keys = {
        env: this.configs.env,
        publicKey: this.configs.publicKey,
        privateKey: this.configs.privateKey,
        accessToken: this.configs.accessToken,
        appId: this.configs.appId,
      };
      const responseUpdateKycAccount = await updateKycAccount(kycInput, keys);
      if (responseUpdateKycAccount.status) {
        if (responseUpdateKycAccount.response?.Account?.KYC?.succeeded) {
          //success -> reload
          const run = `
              window.expoReloadKyc();
              true; // note: this is required, or you'll sometimes get silent failures
            `;
          this._refWebview?.injectJavaScript(run);
        } else {
          Alert.alert(
            'Thông báo',
            responseUpdateKycAccount.response?.Account?.KYC?.message ??
              'Có lỗi từ máy chủ hệ thống'
          );
        }
      } else {
        if (
          responseUpdateKycAccount.response[0]?.extensions?.code ===
          ERROR_CODE.EXPIRED
        ) {
          Alert.alert(
            'Thông báo',
            responseUpdateKycAccount.response[0]?.extensions?.message ??
              'Thông tin xác thực không hợp lệ'
          );
        } else {
          Alert.alert('Thông báo', 'Có lỗi từ máy chủ hệ thống');
        }
      }
    } catch (error) {
      Alert.alert('Thông báo', 'Update KYC Fail');
    } finally {
      // home, reload get kyc
      this.onCloseKyc();
    }
  };

  setLanguage = (language) => {
    this.configs.lang = language;
  };

  render() {
    const {
      modalVisible,
      uri,
      VNPayDataHeader,
      VNPayIsProcessing,
      VNPayIsOpenPopup,
      VNPayDataResult,
      isScanQR,
      kyc: { isKyc, kycMode },
      buttonCloseNapas,
    } = this.state;
    if (VNPayIsProcessing) return <Loading />;

    if (!modalVisible.state) return null;

    return (
      <Modal visible={modalVisible.state} animationType="slide" transparent>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(24,26,65,0.6)',
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}
        >
          {modalVisible.isPay && (
            <View
              style={{
                width: '100%',
                paddingHorizontal: '4%',
                marginBottom: 10,
                alignItems: 'flex-end',
              }}
            >
              <TouchableOpacity
                onPress={() => this.onCloseWebview()}
                activeOpacity={0.7}
                style={{ flexDirection: 'row', alignItems: 'center' }}
              >
                <Image
                  source={Images.iconClose}
                  style={{ width: 16, height: 16 }}
                />
                <Text style={{ color: 'white', marginLeft: 10 }}>
                  {getTranslate('Close', this.configs.lang)}
                </Text>
              </TouchableOpacity>
            </View>
          )}

          {uri !== '' ? (
            <View
              style={
                modalVisible.isPay
                  ? {
                      width: '100%',
                      height: '92%',
                      borderRadius: 15,
                    }
                  : { flex: 1, width: '100%', height: '100%' }
              }
            >
              {buttonCloseNapas && (
                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    top: getStatusBarHeight() + 8,
                    right: 16,
                    zIndex: 999999,
                  }}
                  onPress={() => this.onCloseWebview()}
                >
                  <Image
                    style={{ width: 24, height: 24 }}
                    source={{
                      uri: 'https://img.icons8.com/material-rounded/24/000000/delete-sign.png',
                    }}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              )}
              <WebView
                ref={(ref) => (this._refWebview = ref)}
                // cacheEnabled={false}
                source={{ uri }}
                originWhitelist={['*']}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                allowFileAccess={true}
                allowFileAccessFromFileURLs={true}
                allowUniversalAccessFromFileURLs={true}
                onMessage={this.handleOnMessage}
                style={{
                  borderRadius: modalVisible.isPay ? 15 : 0,
                  flex: 0,
                  height: '100%',
                }}
                containerStyle={{
                  borderRadius: modalVisible.isPay ? 15 : 0,
                  flex: 0,
                  height: '100%',
                }}
                startInLoadingState={true}
                bounces={false}
                onError={(syntheticEvent) =>
                  this.handleErrorWebview(syntheticEvent)
                }
                renderLoading={() => <Loading />}
                renderError={() => (
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: 'transparent',
                      height: '100%',
                    }}
                  >
                    <Text>Lỗi kết nối mạng!</Text>
                  </View>
                )}
              />
            </View>
          ) : null}

          {VNPayIsOpenPopup && (
            <ModalView
              ref={(ref) => (this._refModalVNPay = ref)}
              containerStyle={{ justifyContent: 'flex-end' }}
              style="bottom"
            >
              {VNPayDataResult?.transaction ? (
                <VNPayResult
                  onClose={() => this.closeVNPay()}
                  dataResult={VNPayDataResult}
                  mainColor={this.configs.configColor?.[0]}
                />
              ) : (
                <Processing
                  onClose={() => this.closeVNPay()}
                  color={this.configs.configColor}
                  dataHeader={VNPayDataHeader}
                />
              )}
            </ModalView>
          )}

          {isScanQR && (
            <ModalView
              ref={(ref) => (this._refModalScanQR = ref)}
              style="right"
            >
              <ScanQR
                onClose={this.onCloseScanQR}
                detectQRCode={this.detectQRCode}
                mainColor={this.configs.configColor?.[0]}
                handlePayScanQr={this.handlePayScanQr}
                lang={this.configs.lang}
              />
            </ModalView>
          )}

          {isKyc && (
            <ModalView ref={(ref) => (this._refModalKyc = ref)}>
              <Kyc
                mainColor={this.configs.configColor?.[0]}
                kycMode={kycMode}
                onClose={this.onCloseKyc}
                uploadFile={this.uploadFile}
                updateKycAccount={this.handleUpdateKycAccount}
                updateCCCDAccount={this.handleUpdateCCCDAccount}
              />
            </ModalView>
          )}
        </View>
      </Modal>
    );
  }
}

export { encryptAES, LANGUAGES, ERROR_CODE, ENV, AccountStatus };

export default ExpoPaymeSDK;
