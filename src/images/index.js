export const Images = {
  iconClose: require('./icSetClose.png'),
  iconBack: require('./icArrowBackBlack.png'),
  iconBackWhite: require('./icSetArrowBackWhite.png'),
  iconDropDown: require('./icSetDropDown24Px.png'),
  buttonTakePicture: require('./buttonTakepic.png'),
  iconQrScanNotFound: require('./iconQrScanNotFound.png'),
  artHochieu: require('./artHochieu.png'),
  logoPayME: require('./logoPayME.png'),
  logoPCI: require('./logoPCI.png'),
};
