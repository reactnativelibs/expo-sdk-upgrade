/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect, useRef } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Modal,
  ActivityIndicator,
  Dimensions,
  Linking,
  StatusBar,
  Alert,
} from 'react-native';
import { Camera } from 'expo-camera';
import Constants from 'expo-constants';
import { NotPermissionScreen } from '../components/NotPermissionScreen';
import { Audio } from 'expo-av';
import BarcodeMask from 'react-native-barcode-mask';
import * as ImageManipulator from 'expo-image-manipulator';
import { Confirm } from './Confirm';
import { KYC_MODE } from '../configs/variable.config';
import { IconButton } from './../components/IconButton';
import { Button } from './../components/Button';
import { Images } from '../images';
// import LottieView from "lottie-react-native";
import { Animations } from '../ui';
import { ModalView } from '../components/ModalView';
import { PopupKyc } from './PopupKyc';
import { opacityColorHex } from '../helper/color';
import { ProcessingUpload } from '../components/svgs/ProcessingUpload';
import { ButtonRecord } from '../components/svgs/ButtonRecord';
import { ButtonCapture } from '../components/svgs/ButtonCapture';
import { ArtHoChieu } from '../components/svgs/ArtHoChieu';
import { CardGuild } from '../components/svgs/CardGuild';

const { width, height } = Dimensions.get('window');

const ACTION = {
  CAPTURE: 'CAPTURE',
  VIEW: 'VIEW',
};

const TYPE_IDENTIFY = {
  cmnd: 'Chứng minh nhân dân',
  cccd: 'Căn cước công dân',
  hc: 'Hộ chiếu',
};

export default function Kyc({
  onClose = () => null,
  updateKycAccount = (params) => null,
  updateCCCDAccount = (params) => null,
  uploadFile = async () => null,
  kycMode = {
    isUpdateCCCD: false,
    identifyImg: true,
    faceImg: false,
    kycVideo: false,
  },
  mainColor = '#06d328',
}) {
  const [nextKyc, setNextKyc] = useState(() => {
    if (kycMode.isUpdateCCCD) return KYC_MODE.identifyImg;
    if (kycMode.identifyImg) return KYC_MODE.identifyImg;
    if (kycMode.faceImg) return KYC_MODE.faceImg;
    if (kycMode.kycVideo) return KYC_MODE.kycVideo;
    return KYC_MODE.identifyImg;
  });
  const [showPopUp, setShowPopUp] = useState(() => {
    if (kycMode.isUpdateCCCD) return false;
    return true;
  });
  const refPopupKyc = useRef(null);
  const refScreenView = useRef(null);
  const refButtonRecord = useRef(null);
  const refTimeOutShowCardGuild = useRef(null);

  const [isFront, setIsFont] = useState(true);
  const [imgFront, setImgFont] = useState(null);
  const [imgBack, setImgBack] = useState(null);
  const [imgFace, setImgFace] = useState(null);
  const [videoRecord, setVideoRecord] = useState(null);
  const [recording, setRecording] = useState(false);
  const [uploading, setUploading] = useState(false);
  const [showCardGuild, setShowCardGuild] = useState(false);
  const [showPopupConfirmPassport, setShowPopupConfirmPassport] = useState(
    false
  );

  const listItem = ['Chứng minh nhân dân', 'Căn cước công dân', 'Hộ chiếu'];

  const [identification, setIdentification] = useState(() => {
    if (kycMode.isUpdateCCCD) {
      return 'Căn cước công dân';
    }
    return 'Chứng minh nhân dân';
  });

  const coordinateBarcode = useRef(null);

  const [hasPermissionCamera, setHasPermissionCamera] = useState(null);

  const [hasPermissionAudioRecord, setHasPermissionAudioRecord] = useState(
    null
  );

  const [action, setAction] = useState(null);

  const refCamera = useRef(null);

  const [modalVisible, setModalVisible] = useState(false);

  const [loading, setLoading] = useState(false);

  const [countDown, setCountDown] = useState(5);
  const refCountDown = useRef(null);
  const refInterval = useRef(null);

  const handleRequestPermission = async () => {
    const permissionCamera = await Camera.requestPermissionsAsync();
    if (permissionCamera.status !== 'granted') {
      Linking.openSettings();
    }
    const permissionCameraSecond = await Camera.requestPermissionsAsync();
    setHasPermissionCamera(permissionCameraSecond.status === 'granted');
  };

  useEffect(() => {
    refCountDown.current = 5;
  }, []);

  useEffect(() => {
    if (nextKyc === KYC_MODE.identifyImg) {
      setShowCardGuild(true);
      if (refTimeOutShowCardGuild.current) {
        clearTimeout(refTimeOutShowCardGuild.current);
      }
      refTimeOutShowCardGuild.current = setTimeout(() => {
        setShowCardGuild(false);
      }, 3000);
    }

    return () => {
      if (refTimeOutShowCardGuild.current) {
        clearTimeout(refTimeOutShowCardGuild.current);
      }
    };
  }, [nextKyc, identification, isFront]);

  useEffect(() => {
    if (!showPopUp) {
      if (!hasPermissionCamera) {
        (async () => {
          const permissionCamera = await Camera.requestPermissionsAsync();
          setHasPermissionCamera(permissionCamera.status === 'granted');
        })();
      }
      if (nextKyc === KYC_MODE.kycVideo) {
        if (!hasPermissionAudioRecord) {
          (async () => {
            const permissionAudio = await Audio.requestPermissionsAsync();
            setHasPermissionAudioRecord(permissionAudio.status === 'granted');
          })();
        }
      }
    }
  }, [nextKyc, showPopUp]);

  const handlePressItem = (item) => {
    if (item === TYPE_IDENTIFY.hc) {
      setShowPopupConfirmPassport(true);
      return;
    }
    setIdentification(item);
    setModalVisible(false);
  };

  const getSizeImage = (uri) => {
    return new Promise((resolve) => {
      Image.getSize(
        uri,
        (width, height) => resolve({ width, height }),
        (error) => resolve(null)
      );
    });
  };

  const handleUploadCCCD = async () => {
    setUploading(true);
    try {
      const uploads = [];

      if (imgFront) {
        uploads.push(uploadImage(imgFront?.uri));
      }
      if (imgBack) {
        uploads.push(uploadImage(imgBack?.uri));
      }

      const response = await Promise.all(uploads);
      if (response.includes(false)) {
        // upload fail
        Alert.alert('Thông báo', 'Upload fail'); // -> home
        setUploading(false);
        return;
      } else {
        // upload success -> call api update kyc
        handleUpdateCCCD(response);
      }
    } catch (error) {
      console.log('error upload' + error.message);
    }
  };

  const handleUpdateCCCD = (listPath = []) => {
    const params = {
      identifyIC: {
        front: listPath?.[0] ?? '',
        back: listPath?.[1] ?? '',
      },
    };
    updateCCCDAccount(params);
  };

  const uploadImage = async (localUri) => {
    if (!localUri) return false;
    const filename = localUri.split('/').pop();
    const match = /\.(\w+)$/.exec(filename);
    const type = match ? `image/${match[1]}` : 'image';
    const formData = new FormData();
    formData.append('files', {
      uri: localUri,
      name: filename,
      type,
    });
    const response = await uploadFile(formData);
    if (response?.code === 1000) {
      if (response?.data?.[0]?.state === 'SUCCEEDED') {
        return response?.data?.[0]?.path;
      }
    }
    return false;
  };

  const handleUpdateKycAccount = (listPath = [], listKey = []) => {
    const params = {};
    if (kycMode.identifyImg) {
      params.identifyType =
        identification === TYPE_IDENTIFY.cmnd
          ? 'CMND'
          : identification === TYPE_IDENTIFY.cccd
          ? 'CCCD'
          : 'PASSPORT';
      params.image = { front: listPath[listKey.indexOf('imgFront')] };
      if (listKey.includes('imgBack')) {
        params.image = {
          front: listPath[listKey.indexOf('imgFront')],
          back: listPath[listKey.indexOf('imgBack')],
        };
      }
    }
    if (kycMode.faceImg) {
      params.face = listPath[listKey.indexOf('imgFace')];
    }
    if (kycMode.kycVideo) {
      params.video = listPath[listKey.indexOf('videoRecord')];
    }
    updateKycAccount(params);
  };

  const uploadVideo = async (localUri) => {
    if (!localUri) return false;
    const filename = localUri.split('/').pop();
    const type = Platform.OS === 'ios' ? 'video/quicktime' : 'video/mp4';
    const formData = new FormData();
    formData.append('files', {
      uri: localUri,
      name: filename,
      type,
    });
    const response = await uploadFile(formData);
    if (response?.code === 1000) {
      if (response?.data?.[0]?.state === 'SUCCEEDED') {
        return response?.data?.[0]?.path;
      }
    }
    return false;
  };

  const handleUploadKyc = async () => {
    // console.log("handleUploadKyc");
    setUploading(true);
    try {
      const uploads = [];
      const keyUploads = [];

      if (kycMode.identifyImg) {
        if (imgFront) {
          uploads.push(uploadImage(imgFront?.uri));
          keyUploads.push('imgFront');
        }
        if (imgBack) {
          uploads.push(uploadImage(imgBack?.uri));
          keyUploads.push('imgBack');
        }
      }
      if (kycMode.faceImg) {
        if (imgFace) {
          uploads.push(uploadImage(imgFace?.uri));
          keyUploads.push('imgFace');
        }
      }
      if (kycMode.kycVideo) {
        if (videoRecord) {
          uploads.push(uploadVideo(videoRecord?.uri));
          keyUploads.push('videoRecord');
        }
      }

      const response = await Promise.all(uploads);
      if (response.includes(false)) {
        // upload fail
        Alert.alert('Thông báo', 'Upload fail'); // -> home
        setUploading(false);
        return;
      } else {
        // upload success -> call api update kyc
        handleUpdateKycAccount(response, keyUploads);
      }
    } catch (error) {
      console.log('error upload' + error.message);
    }
  };

  const continueNextKyc = (newNextKyc) => {
    setNextKyc(newNextKyc);
    setShowPopUp(true);
    setAction(ACTION.CAPTURE);
  };

  const handleOnNextKyc = () => {
    if (kycMode.isUpdateCCCD) {
      // update cccd
      if (isFront) {
        //mat truoc
        setAction(ACTION.CAPTURE);
        setIsFont(false);
      } else {
        handleUploadCCCD();
      }
    } else {
      // kyc
      if (
        nextKyc === KYC_MODE.identifyImg &&
        identification !== 'Hộ chiếu' &&
        isFront
      ) {
        // mat truoc giay to
        setAction(ACTION.CAPTURE);
        setIsFont(false);
      } else {
        const newNextKyc = getNextKyc();
        if (newNextKyc) {
          continueNextKyc(newNextKyc);
        } else {
          handleUploadKyc();
        }
      }
    }
  };

  const handlenNavigateConfirm = (picture, video) => {
    if (nextKyc === KYC_MODE.identifyImg) {
      if (isFront) {
        setImgFont(picture);
      } else {
        setImgBack(picture);
      }
    } else if (nextKyc === KYC_MODE.faceImg) {
      setImgFace(picture);
    } else {
      setVideoRecord(video);
    }
    setAction(ACTION.VIEW);
  };

  const getNextKyc = () => {
    let newNextKyc = '';
    if (nextKyc === KYC_MODE.identifyImg) {
      newNextKyc = kycMode.faceImg
        ? KYC_MODE.faceImg
        : kycMode.kycVideo
        ? KYC_MODE.kycVideo
        : '';
    } else if (nextKyc === KYC_MODE.faceImg) {
      newNextKyc = kycMode.kycVideo ? KYC_MODE.kycVideo : '';
    }
    return newNextKyc;
  };

  const capture = async () => {
    // setLoading(true);
    try {
      let picture = null;
      if (nextKyc === KYC_MODE.identifyImg) {
        const pic = await refCamera.current?.takePictureAsync({
          quality: 1,
        });
        const size = await getSizeImage(pic.uri);
        const scaleRatio = size.width / width;

        picture = await ImageManipulator.manipulateAsync(
          pic.uri,
          [
            {
              crop: {
                originX: coordinateBarcode.current?.x * scaleRatio,
                originY:
                  (coordinateBarcode.current?.y + StatusBar.currentHeight) *
                  scaleRatio *
                  (Platform.OS === 'ios' ? 1 : 16 / 9),
                width: (coordinateBarcode.current?.width * pic.width) / width,
                height: (coordinateBarcode.current?.height * pic.width) / width,
              },
            },
          ],
          { compress: 1, format: ImageManipulator.SaveFormat.PNG }
        );
        handlenNavigateConfirm(picture, null);
      } else {
        // face
        picture = await refCamera.current?.takePictureAsync({
          quality: 0.6,
        });
        handlenNavigateConfirm(picture, null);
      }
    } catch (error) {
      console.log(error.message ?? 'error capture');
    } finally {
      setLoading(false);
    }
  };
  // useEffect(() => {setRecording(false)},[])
  const runCountDown = () => {
    refCountDown.current = 5;
    setCountDown(5);
    refInterval.current = setInterval(() => {
      const newCountDown = refCountDown.current - 1;
      if (newCountDown > -1) {
        refCountDown.current = newCountDown;
        setCountDown(newCountDown);
      } else {
        if (refInterval.current) clearInterval(refInterval.current);
      }
    }, 1000);
  };

  const record = async () => {
    //permission
    if (hasPermissionAudioRecord !== 'granted') {
      const permissionAudio = await Audio.requestPermissionsAsync();
      setHasPermissionAudioRecord(permissionAudio.status !== 'granted');
      if (permissionAudio.status !== 'granted') {
        Linking.openSettings();
        return;
      }
    }
    setRecording(true);
    // refButtonRecord.current?.play();
    runCountDown();
    //record
    refCamera.current
      ?.recordAsync({ mute: true, maxDuraion: 6 })
      .then(async (video) => {
        setRecording(false);
        handlenNavigateConfirm(null, video);
      })
      .catch((e) => {
        setRecording(false);
        console.log(e.message);
      });

    //stop
    setTimeout(() => {
      refCamera.current?.stopRecording();
      // setRecording(false);
    }, 6000);
  };

  const handleClose = () => onClose?.();

  const getTitle = () => {
    if (nextKyc === KYC_MODE.faceImg) return 'Chụp ảnh khuôn mặt';
    if (nextKyc === KYC_MODE.kycVideo) return 'Quay video xác thực';
    return 'Chụp ảnh giấy tờ';
  };

  const renderLoading = () => {
    if (!loading) return null;
    return (
      <View style={styles.loadingView}>
        <ActivityIndicator size="large" color="green" />
      </View>
    );
  };

  const renderModalIdentification = () => {
    if (!modalVisible) return null;
    return (
      <Modal visible={modalVisible} transparent animationType="slide">
        <View style={styles.modalContainer}>
          <View
            style={{
              width: '100%',
              paddingVertical: 16,
              backgroundColor: 'white',
              borderRadius: 15,
              marginBottom: 12,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 10,
              }}
            >
              <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                Chọn loại giấy tờ
              </Text>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ position: 'absolute', right: 16 }}
                onPress={() => setModalVisible(false)}
              >
                <Image source={require('../images/icSetClose16Px.png')} />
              </TouchableOpacity>
            </View>

            {listItem.map((item, index) => (
              <TouchableOpacity
                onPress={() => handlePressItem(item)}
                activeOpacity={0.7}
                key={index.toString()}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderBottomWidth: index + 1 !== listItem.length ? 1 : 0,
                  paddingVertical: 10,
                  borderBottomColor: 'rgb(236, 236, 236)',
                  backgroundColor:
                    item === identification ? 'rgba(255,103,0, 0.1)' : 'white',
                }}
              >
                <Text style={{ fontSize: 16 }}>{item}</Text>
                <View style={{ position: 'absolute', right: 16 }}>
                  {item === identification ? (
                    <Image
                      source={require('../images/inputRadioChecked.png')}
                    />
                  ) : (
                    <Image
                      source={require('../images/inputRadioUncheck.png')}
                    />
                  )}
                </View>
              </TouchableOpacity>
            ))}
          </View>
          {renderModalConfirmPassport()}
        </View>
      </Modal>
    );
  };

  const renderModalConfirmPassport = () => {
    const onPressAgree = () => {
      setShowPopupConfirmPassport(false);
      setIdentification(TYPE_IDENTIFY.hc);
      setModalVisible(false);
    };
    const onPressChange = () => {
      setShowPopupConfirmPassport(false);
    };
    if (!showPopupConfirmPassport) return null;
    return (
      <View style={styles.layoutPopupConfitmPassport}>
        <View style={styles.popupConfitmPassport}>
          {/* <Image source={Images.artHochieu} style={{ width: 146, height: 126 }} /> */}
          <ArtHoChieu color={mainColor} />
          <Text style={styles.txtDescPopupConfirmPassport}>
            Hộ chiếu chỉ dùng để cung cấp định danh cho người nước ngoài
          </Text>
          <View style={{ width: '100%', flexDirection: 'row' }}>
            <Button
              mainColor={mainColor}
              text={'Đồng ý'}
              styles={{ flex: 1 }}
              onPress={onPressAgree}
            />
            <Button
              mainColor={mainColor}
              text={'Thay đổi'}
              styles={{
                flex: 1,
                marginLeft: 10,
                backgroundColor: opacityColorHex(mainColor, 0.1),
              }}
              textStyle={{ color: mainColor }}
              onPress={onPressChange}
            />
          </View>
        </View>
      </View>
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <IconButton
          icon={
            <Image
              source={Images.iconBackWhite}
              style={{ width: 32, height: 32 }}
            />
          }
          style={{ position: 'absolute', left: 0 }}
          onPress={() => {
            if (recording) return null;
            onClose();
          }}
        />

        <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>
          {getTitle()}
        </Text>
      </View>
    );
  };

  const renderCardGuild = () => {
    if (!showCardGuild) return null;

    return (
      <View style={styles.layoutCardGuild}>
        <View style={styles.cardGuildContainer}>
          <Text style={styles.txtTitleCardGuild}>
            {isFront ? 'Mặt trước' : 'Mặt sau'}
          </Text>
          <CardGuild color={mainColor} />
        </View>
      </View>
    );
  };

  const renderCameraAndBarcode = () => {
    return (
      <View style={styles.layoutCameraContainer}>
        {nextKyc === KYC_MODE.kycVideo ? (
          <Camera
            style={{ flex: 1 }}
            type={Camera.Constants.Type.front}
            ref={(ref) => (refCamera.current = ref)}
            ratio="16:9"
            autoFocus={Camera.Constants.AutoFocus.on}
          />
        ) : (
          <Camera
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            type={
              nextKyc === KYC_MODE.faceImg
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back
            }
            flashMode={Camera.Constants.FlashMode.auto}
            ref={(ref) => (refCamera.current = ref)}
            ratio="16:9"
            autoFocus={Camera.Constants.AutoFocus.on}
          />
        )}

        {nextKyc === KYC_MODE.identifyImg && (
          <BarcodeMask
            onLayoutMeasured={(event) => {
              coordinateBarcode.current = event.nativeEvent.layout;
            }}
            showAnimatedLine={false}
            edgeBorderWidth={2}
            width={width * 0.9}
            height={230}
          />
        )}
        {nextKyc === KYC_MODE.identifyImg && renderCardGuild()}
      </View>
    );
  };

  if (uploading) {
    return (
      <View style={styles.uploadingContainer}>
        <ProcessingUpload />
        {/* <LottieView source={Animations.processingUpload} style={{ width: "100%" }} autoPlay loop /> */}
        <View
          style={{ position: 'absolute', bottom: '10%', alignItems: 'center' }}
        >
          <Text style={{ color: 'white', fontSize: 22, fontWeight: '600' }}>
            Đang tải dữ liệu…
          </Text>
          <Text style={{ color: 'white', fontSize: 16 }}>
            Vui lòng chờ trong giây lát
          </Text>
        </View>
      </View>
    );
  }

  if (showPopUp) {
    return (
      <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.7)' }}>
        <ModalView ref={refPopupKyc}>
          <PopupKyc
            type={nextKyc}
            onPressButtonSubmit={() => {
              refPopupKyc.current?.close(() => {
                setShowPopUp(false);
              });
            }}
            onClose={onClose}
            mainColor={mainColor}
          />
        </ModalView>
      </View>
    );
  }

  if (hasPermissionCamera === null) {
    return null;
  }

  if (hasPermissionCamera === false) {
    return (
      <NotPermissionScreen
        handleClose={handleClose}
        handleRequestPermission={handleRequestPermission}
      />
    );
  }

  if (action === ACTION.VIEW)
    return (
      <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.7)' }}>
        <ModalView ref={refScreenView} style="right">
          <View style={styles.container}>
            <Confirm
              data={{
                mode: nextKyc,
                isFront: isFront,
                imgFont: imgFront,
                imgBack: imgBack,
                imgFace: imgFace,
                videoRecord: videoRecord,
                isDone: kycMode.isUpdateCCCD
                  ? isFront
                    ? false
                    : true
                  : getNextKyc()
                  ? false
                  : true,
              }}
              onBack={() => {
                setAction(ACTION.CAPTURE);
              }}
              onSubmit={handleOnNextKyc}
              mainColor={mainColor}
            />
          </View>
        </ModalView>
      </View>
    );

  return (
    <View style={styles.container}>
      {renderCameraAndBarcode()}

      <View style={{ flex: 1, marginHorizontal: 10 }}>
        {renderHeader()}

        <View
          style={{ flex: 1, justifyContent: 'space-between', marginTop: 20 }}
        >
          {nextKyc === KYC_MODE.identifyImg && (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              {isFront && !kycMode.isUpdateCCCD ? (
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={{ flexDirection: 'row', alignItems: 'center' }}
                  onPress={() => setModalVisible(true)}
                >
                  <Text
                    style={{ color: 'white', fontSize: 16, marginRight: 5 }}
                  >
                    {identification}
                  </Text>
                  <Image
                    source={Images.iconDropDown}
                    style={{ width: 18, height: 18 }}
                  />
                </TouchableOpacity>
              ) : (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text
                    style={{ color: 'white', fontSize: 16, marginRight: 5 }}
                  >
                    {identification}
                  </Text>
                </View>
              )}

              <Text style={{ color: 'white', fontSize: 16 }}>
                {isFront ? 'Mặt trước' : 'Mặt sau'}
              </Text>
            </View>
          )}

          {nextKyc === KYC_MODE.kycVideo && (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text
                style={{ color: 'white', fontSize: 16, textAlign: 'center' }}
              >
                Giữ gương mặt và mặt trước giấy tờ tuỳ thân trước ống kính máy
                quay
              </Text>
            </View>
          )}

          {nextKyc === KYC_MODE.faceImg && (
            <View style={{ width: '100%', alignItems: 'center' }}>
              <View
                style={{
                  height: width * 0.85,
                  width: width * 0.85,
                  borderWidth: 2,
                  borderColor: 'white',
                  borderRadius: width * 0.5,
                  marginTop: '10%',
                }}
              />
            </View>
          )}

          <View>
            {nextKyc !== KYC_MODE.kycVideo && (
              <Text
                style={{
                  color: 'white',
                  fontSize: 14,
                  textAlign: 'center',
                  marginBottom: (height - 360) / 2 - 60,
                }}
              >
                {nextKyc === KYC_MODE.faceImg
                  ? 'Vui lòng giữ gương mặt trong khung tròn'
                  : 'Vui lòng cân chỉnh giấy tờ tùy thân vào giữa khung'}
              </Text>
            )}

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 20,
                height: 80,
              }}
            >
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={{
                    marginBottom: 5,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={
                    nextKyc === KYC_MODE.kycVideo
                      ? recording
                        ? null
                        : record
                      : capture
                  }
                >
                  {nextKyc === KYC_MODE.kycVideo ? (
                    // <LottieView
                    //   ref={refButtonRecord}
                    //   source={Animations.kycDangquay_5}
                    //   style={{ width: 160 }}
                    //   loop={false}
                    //   colorFilters={[{ keypath: "Camera", color: mainColor }]}
                    // />
                    <>
                      {recording ? (
                        <View style={styles.recording}>
                          <Text
                            style={{
                              color: 'white',
                              fontSize: 26,
                              fontWeight: 'bold',
                            }}
                          >
                            {countDown}
                          </Text>
                        </View>
                      ) : (
                        <ButtonRecord color={mainColor} />
                      )}
                    </>
                  ) : (
                    <ButtonCapture color={mainColor} />
                    // <Image source={Images.buttonTakePicture} style={{ width: 80, height: 80 }} />
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>

      {renderModalIdentification()}
      {renderLoading()}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    paddingTop: Platform.OS === 'ios' ? Constants.statusBarHeight : 0,
  },
  camera: {
    flex: 1,
  },
  recording: {
    width: 70,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 35,
    borderWidth: 0.5,
    borderColor: 'white',
  },
  loadingView: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.8)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.8)',
    alignItems: 'center',
    justifyContent: 'flex-end',
    // padding: 16,
  },
  layoutCameraContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'red',
  },
  header: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  uploadingContainer: {
    flex: 1,
    backgroundColor: 'rgb(12, 170, 38)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  layoutPopupConfitmPassport: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.9)',
  },
  popupConfitmPassport: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 12,
    alignItems: 'center',
  },
  txtDescPopupConfirmPassport: {
    fontSize: 17,
    fontWeight: '600',
    textAlign: 'center',
    width: '90%',
    marginBottom: 20,
    marginTop: 30,
  },
  layoutCardGuild: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardGuildContainer: {
    width: '100%',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtTitleCardGuild: {
    color: 'white',
    fontSize: 16,
    marginBottom: 5,
  },
});
