/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Images } from '../images';
import { IconButton } from './../components/IconButton';
import { Button } from './../components/Button';
import { getAnimationKYC } from '../helper/getAnimationColor';
import { opacityColorHex } from '../helper/color';
import { KYC_MODE } from '../configs/variable.config';

export const PopupKyc = ({
  type = KYC_MODE.identifyImg,
  onClose = () => null,
  onPressButtonSubmit = () => null,
  mainColor = '#06d328',
}) => {
  const CheckBulletData = {
    identifyImg: [
      '1. Sử dụng giấy tờ tuỳ thân hợp lệ của chính bạn',
      '2. Phải đặt thẻ bên trong khung camera',
      '3. Không sử dụng bản sao của giấy tờ tuỳ thân',
    ],
    kycVideo: [
      '1. Video sẽ có độ dài 5 giây',
      '2. Vui lòng giữ gương mặt và mặt trước giấy tờ tùy thân của bạn trước máy quay',
    ],
    faceImg: [
      '1. Vui lòng giữ gương mặt ở trong khung tròn',
      '2. Giữ cho ảnh sắc nét không bị nhoè',
    ],
  };

  const renderTitle = () => {
    switch (type) {
      case KYC_MODE.identifyImg:
        return 'Chụp hình giấy tờ tuỳ thân';
      case KYC_MODE.faceImg:
        return 'Chụp ảnh xác thực khuôn mặt';
      case KYC_MODE.kycVideo:
        return 'Quay video xác thực';

      default:
        return '--';
    }
  };
  const renderSubTitle = () => {
    if (type !== KYC_MODE.identifyImg) return null;
    return (
      <View
        style={[
          styles.subTitle,
          { backgroundColor: opacityColorHex(mainColor, 0.1) },
        ]}
      >
        <Text style={{ color: '#949494', textAlign: 'center' }}>
          Theo quy định của NHNN, tài khoản Ví điện tử phải được định danh trước
          khi sử dụng.
        </Text>
      </View>
    );
  };
  const renderAnimation = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginVertical: 10,
        }}
      >
        {getAnimationKYC(type, mainColor)}
      </View>
    );
  };
  const renderCheckBullet = () => {
    return (
      <View style={styles.CheckBulletContainer}>
        {CheckBulletData[type]?.map((text, index) => (
          <Text
            style={{ fontSize: 14, marginBottom: 5 }}
            key={index.toString()}
          >
            {text}
          </Text>
        ))}
      </View>
    );
  };
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.txtTitle}>{renderTitle()}</Text>
          <IconButton
            icon={
              <Image
                source={Images.iconClose}
                style={{ width: 16, height: 16 }}
              />
            }
            style={styles.iconClose}
            onPress={onClose}
          />
        </View>

        {renderSubTitle()}
        {renderAnimation()}
        {renderCheckBullet()}

        <Button
          text="TIẾP TỤC "
          onPress={onPressButtonSubmit}
          mainColor={mainColor}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '96%',
    backgroundColor: '#fff',
    borderRadius: 15,
    padding: 12,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 12,
  },
  txtTitle: {
    fontSize: 18,
    fontWeight: '600',
  },
  iconClose: {
    position: 'absolute',
    right: 0,
  },
  CheckBulletContainer: {
    padding: 12,
    marginBottom: 5,
  },
  subTitle: {
    borderRadius: 20,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: 'rgba(255, 0, 0, 0.1)',
  },
});
