import React from 'react';
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import { Images } from '../images';
import { IconButton } from './../components/IconButton';
import { Button } from './../components/Button';
import { isIphoneX } from '../helper/iphoneXHelper';
import { Video } from 'expo-av';
import { KYC_MODE } from '../configs/variable.config';

export const Confirm = ({
  onBack = () => null,
  onSubmit = () => null,
  data = {
    mode: KYC_MODE.identifyImg,
    isFront: true,
    imgFont: null,
    imgBack: null,
    imgFace: null,
    videoRecord: null,
    isDone: false,
  },
  mainColor = '#06d328',
}) => {
  const type = data.mode === KYC_MODE.kycVideo ? 'video' : 'image';
  const getTextButtonSubmit = () => {
    return data.isDone ? 'Hoàn tất' : 'Tiếp tục';
  };

  const getTitle = () => {
    return type === 'image'
      ? 'Xác nhận ảnh'
      : type === 'video'
      ? 'Xác nhận video'
      : '--';
  };

  const getDecription = () => {
    const txtVideo =
      'Vui lòng xác nhận video đã rõ nét, gương mặt và giấy tờ tùy thân của bạn đã có trong khung hình.';
    const txtImage =
      'Vui lòng xác nhận ảnh  đã rõ ràng, gương mặt của bạn có trong khung hình.';
    return type === 'image' ? txtImage : type === 'video' ? txtVideo : '--';
  };

  const getUriImage = () => {
    if (data.mode === KYC_MODE.identifyImg) {
      if (data.isFront) {
        return data.imgFont?.uri ?? '';
      }
      return data.imgBack?.uri ?? '';
    } else {
      return data.imgFace?.uri ?? '';
    }
  };

  const getSizeView = () => {
    const { width, height } = Dimensions.get('window');
    if (data.mode === KYC_MODE.identifyImg) {
      const w = width - 40;
      const image = data.isFront ? data.imgFont : data.imgBack;
      const h = (w / (image?.width ?? 1)) * (image?.height ?? 0);
      return {
        width: w,
        height: h >= 250 ? 250 : h,
      };
    } else if (data.mode === KYC_MODE.faceImg) {
      const h = 400;
      const w = width - 40;
      return {
        width: w,
        height: h,
      };
    } else {
      return {
        width: 400,
        height: 200,
      };
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <IconButton
          icon={
            <Image source={Images.iconBack} style={{ width: 32, height: 32 }} />
          }
          style={styles.iconBack}
          onPress={onBack}
        />
        <Text style={styles.txtTitle}>{getTitle()}</Text>
      </View>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View
          style={{
            backgroundColor: 'rgba(203, 203, 203, 0.5)',
            width: '100%',
            padding: 10,
            borderRadius: 10,
          }}
        >
          {type === 'image' ? (
            <Image
              style={getSizeView()}
              source={{
                uri: getUriImage(),
              }}
              resizeMode="contain"
            />
          ) : (
            <Video
              style={{
                alignSelf: 'center',
                width: '100%',
                height: 400,
              }}
              source={{
                uri: data.videoRecord?.uri ?? '',
              }}
              useNativeControls
              resizeMode="contain"
              isLooping
            />
          )}
        </View>
        <Text
          style={{
            fontSize: 16,
            fontWeight: '600',
            textAlign: 'center',
            marginTop: 16,
          }}
        >
          {getDecription()}
        </Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <Button
          mainColor={mainColor}
          text="Chụp lại"
          outline={true}
          styles={{ flex: 1 }}
          onPress={onBack}
        />
        <Button
          mainColor={mainColor}
          text={getTextButtonSubmit()}
          styles={{ flex: 1, marginLeft: 10 }}
          onPress={onSubmit}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
    paddingBottom: isIphoneX() ? 16 : 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtTitle: {
    fontSize: 18,
    fontWeight: '700',
  },
  iconBack: {
    position: 'absolute',
    left: 0,
  },
});
