import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Images } from '../images';
import { Button } from '../components';
import { IconNoticeSuccesss } from '../components/svgs/IconNoticeResultSuccess';
import { IconNoticePending } from '../components/svgs/IconNoticeResultPending';
import { IconNoticeFailed } from '../components/svgs/IconNoticeResultFailed';
import { insertCurrency } from '../helper/currencyFormat';
import { TransactionInfo } from './items/TransactionInfo';
import moment from 'moment';

export const VNPayResult = ({ onClose, dataResult, mainColor }) => {
  const getTitle = (status) => {
    if (status === 'SUCCEEDED') {
      return 'Thanh toán thành công';
    }
    if (
      status === 'PENDING' ||
      status === 'PROCESSING' ||
      status === 'WAITING'
    ) {
      return 'Đang chờ xử lý';
    }
    if (status === 'FAILED') {
      return 'Thanh toán thất bại';
    }
    return 'Đang chờ xử lý';
  };

  const renderIcon = (status) => {
    if (status === 'SUCCEEDED') {
      return <IconNoticeSuccesss color={mainColor} />;
    }
    if (
      status === 'PENDING' ||
      status === 'PROCESSING' ||
      status === 'WAITING'
    ) {
      return <IconNoticePending />;
    }
    if (status === 'FAILED') {
      return <IconNoticeFailed />;
    }
    return <IconNoticeSuccesss />;
  };

  const getData1 = () => {
    const data = [
      {
        label: 'Mã giao dịch',
        value: dataResult?.transaction,
      },
      {
        label: 'Thời gian giao dịch',
        value: moment(dataResult.createdAt).format('HH:mm DD/MM/YYYY'),
      },
      {
        label: 'Phương thức',
        value: 'QR Pay',
      },
      {
        label: 'Phí giao dịch',
        value:
          dataResult?.fee === 0
            ? 'Miễn phí'
            : `${insertCurrency(dataResult?.fee)} đ`,
      },
      {
        label: 'Tổng thanh toán',
        value: `${insertCurrency(dataResult?.total)} đ`,
        valueStyle: { color: '#ec2a2a' },
      },
    ];

    // remove(data, (item) => item?.label === 'Mã giao dịch' && !item?.value)
    // remove(data, (item) => item?.label === 'Phí giao dịch' && !item?.value)
    // remove(data, (item) => item?.label === 'Tổng thanh toán' && !item?.value)

    return data;
  };

  const information2 = [
    {
      label: 'Người nhận',
      value: dataResult?.storeName ?? '--',
    },
    {
      label: 'Nội dung',
      value: dataResult?.description ?? '--',
    },
  ];

  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        scrollEnabled={true}
        contentContainerStyle={{
          flexGrow: 1,
        }}
        keyboardShouldPersistTaps="handled"
        keyboardDismissMode="interactive"
        bounces={false}
      >
        <View style={styles.statusView}>
          {renderIcon(dataResult?.state)}
          <Text style={styles.statusTitle}>{getTitle(dataResult?.state)}</Text>
          <Text style={styles.amountText}>{`${insertCurrency(
            dataResult?.total
          )} đ`}</Text>
          {!!dataResult?.message && dataResult?.state === 'FAILED' && (
            <Text style={styles.textMessageError}>{dataResult?.message}</Text>
          )}
          {!dataResult?.message &&
            (dataResult?.state === 'PENDING' ||
              dataResult?.state === 'PROCESSING' ||
              dataResult?.state === 'WAITING') && (
              <View style={styles.containerPending}>
                <Text style={styles.textPending}>
                  Cần thời gian thêm để xử lý. Vui lòng không thực hiện lại
                  tránh bị trùng. Liên hệ CSKH để hỗ trợ
                </Text>
                <Text style={styles.textCSKHPending}>1900 88 66 65</Text>
              </View>
            )}
        </View>
        <View style={styles.infoView}>
          <TransactionInfo
            content={getData1()}
            lableStyleAll={styles.styleLabel}
            valueStyleAll={styles.styleValue}
          />
          <TransactionInfo
            content={information2}
            containerStyle={{ marginTop: 0 }}
            lableStyleAll={styles.styleLabel}
            valueStyleAll={styles.styleValue}
          />
        </View>
      </ScrollView>
      <View style={styles.button}>
        <Button mainColor={mainColor} text="Đã hiểu" onPress={onClose} />
      </View>
      <View style={styles.footer}>
        <View style={styles.paymeView}>
          <Text style={styles.textPayme}>Powered by</Text>
          <Image
            style={styles.logoPayme}
            source={Images.logoPayME}
            resizeMode="contain"
          />
        </View>
        <View style={styles.pciView}>
          <Text style={styles.textPCI}>Bảo mật với chứng chỉ </Text>
          <Image
            style={styles.logoPCI}
            source={Images.logoPCI}
            resizeMode="contain"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'flex-end',
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    height: '90%',
  },
  statusView: {
    padding: 16,
    alignItems: 'center',
  },
  statusTitle: {
    fontWeight: '600',
    fontSize: 21,
    color: '#000050',
    marginTop: 16,
    marginBottom: 4,
  },
  amountText: {
    fontWeight: '600',
    fontSize: 36,
    color: '#000050',
  },
  containerPending: {
    marginTop: 5,
    marginHorizontal: 16,
    marginBottom: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textMessageError: {
    fontSize: 16,
    textAlign: 'center',
    color: '#d0021b',
    marginTop: 5,
    marginBottom: 12,
    marginHorizontal: 16,
  },
  textPending: {
    fontSize: 14,
    textAlign: 'center',
    color: '#d0021b',
    marginBottom: 8,
  },
  textCSKHPending: {
    fontSize: 18,
    fontWeight: '600',
    color: '#181A41',
  },
  infoView: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingHorizontal: 16,
    backgroundColor: '#eff2f7',
    flex: 1,
    width: '100%',
  },
  box: {
    backgroundColor: '#ffffff',
    padding: 16,
    margin: 16,
    borderRadius: 16,
  },
  styleLabel: {
    fontSize: 15,
    color: '#0b0b0b',
  },
  styleValue: {
    fontWeight: '600',
    fontSize: 15,
    color: '#0b0b0b',
  },
  button: {
    padding: 16,
    backgroundColor: '#ffffff',
  },
  footer: {
    backgroundColor: '#eff2f7',
    paddingHorizontal: 16,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  paymeView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textPayme: {
    fontSize: 10.8,
    color: '#647081',
    fontWeight: '700',
  },
  logoPayme: {
    marginLeft: 4,
  },
  pciView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textPCI: {
    fontSize: 11,
    color: '#647081',
    fontStyle: 'italic',
  },
  logoPCI: {
    marginLeft: 4,
  },
});
