import React from 'react';
import { ActivityIndicator, Image, StyleSheet, Text, View } from 'react-native';
import { Button } from '../components';
import { Images } from '../images';
import { LinearGradient } from 'expo-linear-gradient';
import { insertCurrency } from '../helper/currencyFormat';
import { LineDot } from './items/LineDot';

export const Processing = ({ onClose, color, dataHeader }) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <LinearGradient
          // Button Linear Gradient
          colors={color}
          start={[0, 0]}
          end={[1, 1]}
          style={styles.headerGradient}
        >
          {dataHeader?.isVisibleHeader ? (
            <View>
              <View
                style={[
                  styles.boxAmount,
                  {
                    justifyContent: dataHeader?.storeImage
                      ? 'space-between'
                      : 'center',
                  },
                ]}
              >
                {dataHeader?.storeImage && (
                  <Image
                    source={{ uri: dataHeader?.storeImage }}
                    style={{ width: 56, height: 56, borderRadius: 28 }}
                  />
                )}
                <View style={{ flexDirection: 'column' }}>
                  <Text style={[styles.amountTitle, { textAlign: 'right' }]}>
                    Số tiền thanh toán
                  </Text>
                  <Text
                    style={[styles.amount, { textAlign: 'right' }]}
                  >{`${insertCurrency(dataHeader?.amount)} đ`}</Text>
                </View>
              </View>
              <LineDot style={{ marginVertical: 10 }} />
              <View style={styles.boxContent}>
                <View style={styles.note}>
                  <Text style={styles.noteText}>Người nhận</Text>
                  <Text style={[styles.noteTextValue, { fontWeight: '700' }]}>
                    {dataHeader?.storeName}
                  </Text>
                </View>
                <View style={styles.note}>
                  <Text style={styles.noteText}>Mã dịch vụ</Text>
                  <Text style={styles.noteTextValue}>
                    {dataHeader?.orderId}
                  </Text>
                </View>
                <View style={styles.note}>
                  <Text style={styles.noteText}>Nội dung</Text>
                  <Text style={styles.noteTextValue}>
                    {dataHeader?.note ?? 'Không có nội dung'}
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <View style={[styles.boxAmount, { justifyContent: 'center' }]}>
              <View style={{ flexDirection: 'column' }}>
                <Text style={styles.textHeader1}>Số tiền thanh toán</Text>
                <Text style={styles.textHeader2}>{`${insertCurrency(
                  dataHeader?.amount
                )} đ`}</Text>
              </View>
            </View>
          )}
        </LinearGradient>
      </View>

      <View style={styles.center}>
        <Text style={styles.textCenter}>Giao dịch đang được xử lý</Text>
        <ActivityIndicator size="large" color={color?.[0]} />
      </View>
      <View style={styles.button}>
        <Button
          mainColor={color?.[0]}
          outline={true}
          text="Huỷ giao dịch"
          textStyle={{ fontWeight: '400' }}
          onPress={onClose}
        />
      </View>
      <View style={styles.footer}>
        <View style={styles.paymeView}>
          <Text style={styles.textPayme}>Powered by</Text>
          <Image
            style={styles.logoPayme}
            source={Images.logoPayME}
            resizeMode="contain"
          />
        </View>
        <View style={styles.pciView}>
          <Text style={styles.textPCI}>Bảo mật với chứng chỉ </Text>
          <Image
            style={styles.logoPCI}
            source={Images.logoPCI}
            resizeMode="contain"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'flex-end',
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  header: {
    width: '100%',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    overflow: 'hidden',
  },
  headerGradient: {
    width: '100%',
    padding: 16,
  },
  amountTitle: {
    fontSize: 14,
    color: '#ffffff',
    textAlign: 'center',
    fontWeight: '600',
  },
  amount: {
    fontSize: 26,
    color: '#ffffff',
    textAlign: 'center',
  },
  boxAmount: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  boxContent: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  note: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 4,
  },
  noteText: {
    fontSize: 14,
    color: '#ffffff',
  },
  noteTextValue: {
    fontSize: 14,
    color: '#ffffff',
  },
  textHeader1: {
    fontSize: 14,
    fontWeight: '600',
    color: '#ffffff',
  },
  textHeader2: {
    fontSize: 26,
    fontWeight: '700',
    color: '#ffffff',
  },
  center: {
    alignItems: 'center',
    padding: 16,
  },
  textCenter: {
    fontSize: 20,
    fontWeight: '600',
    marginBottom: 16,
  },
  button: {
    padding: 16,
    backgroundColor: '#ffffff',
  },
  footer: {
    backgroundColor: '#eff2f7',
    paddingHorizontal: 16,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  paymeView: {
    flexDirection: 'row',
  },
  textPayme: {
    fontSize: 10.8,
    color: '#647081',
    fontWeight: '700',
  },
  logoPayme: {
    marginLeft: 4,
  },
  pciView: {
    flexDirection: 'row',
  },
  textPCI: {
    fontSize: 11,
    color: '#647081',
    fontStyle: 'italic',
  },
  logoPCI: {
    marginLeft: 4,
  },
});
