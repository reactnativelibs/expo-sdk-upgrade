import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { Line as LineSVG } from 'react-native-svg';

export const LineDot = (props) => {
  return (
    <Svg width="100%" height="2" style={[styles.svg, props.style]}>
      <LineSVG
        x1="1"
        x2="100%"
        y1="1"
        y2="1"
        stroke="#D6D6D6"
        strokeWidth="1"
        strokeLinecap="round"
        strokeDasharray="1, 5"
      />
    </Svg>
  );
};

const styles = StyleSheet.create({
  svg: {
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
});
