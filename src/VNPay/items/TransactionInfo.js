import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { LineDot } from './LineDot';

export const TransactionInfo = ({
  children,
  content,
  containerStyle,
  lableStyleAll,
  valueStyleAll,
}) => {
  const checkLastItem = (array, item) => {
    if (array[array.length - 1]?.label === item?.label) {
      return true;
    } else {
      return false;
    }
  };

  const renderItemValue = (item) => {
    if (item?.isImage) {
      return (
        <Image
          style={styles.imgContent}
          source={{ uri: item.value }}
          resizeMode="contain"
        />
      );
    }
    return (
      <Text
        style={[
          styles.textValueRow,
          valueStyleAll,
          item.styleValue,
          item.valueStyle,
        ]}
      >
        {item?.value ?? 'N/A'}
      </Text>
    );
  };

  return (
    <View>
      {content && (
        <View style={[styles.containerContent, containerStyle]}>
          {content.map((item, index) => {
            if (item?.label) {
              return (
                <View key={index}>
                  <View style={styles.containerTextRow}>
                    <Text
                      style={[
                        styles.textTitleRow,
                        lableStyleAll,
                        item.styleLabel,
                      ]}
                    >
                      {item.label}
                    </Text>
                    <View
                      style={[styles.valueContainer, item.backgroundStyleValue]}
                    >
                      {renderItemValue(item)}
                    </View>
                  </View>
                  {checkLastItem(content, item) ? null : <LineDot />}
                </View>
              );
            }
            return null;
          })}
          {children}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  containerContent: {
    backgroundColor: '#ffffff',
    marginTop: 16,
    borderRadius: 15,
    paddingHorizontal: 16,
    paddingVertical: 12,
    marginBottom: 16,
  },
  containerTextRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 8,
  },
  textTitleRow: {
    fontSize: 16,
    color: '#0b0b0b',
  },
  textValueRow: {
    flex: 1,
    textAlign: 'right',
    marginLeft: 10,
    fontSize: 16,
    fontWeight: '500',
    color: '#0b0b0b',
  },
  valueContainer: {
    flex: 1,
    marginLeft: 10,
    alignItems: 'flex-end',
  },
  imgContent: {
    width: 28,
    height: 28,
  },
});
