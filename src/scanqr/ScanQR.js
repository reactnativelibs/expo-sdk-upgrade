/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  Dimensions,
  ActivityIndicator,
  Linking,
} from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Camera } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import BarcodeMask from 'react-native-barcode-mask';
import { NotPermissionScreen } from '../components/NotPermissionScreen';
import { IconButton } from './../components';
import { Images } from '../images';
import { PopupScanQrNotFound } from './PopupScanQrNotFound';
import { getTranslate } from '../translates';

const { width } = Dimensions.get('screen');
const widthBarcode = (width * 65) / 100;

export default function ScanQr({
  onClose,
  detectQRCode = async () => null,
  mainColor,
  handlePayScanQr = async () => null,
  lang = 'vi',
}) {
  const [hasPermissionCamera, setHasPermissionCamera] = useState(null);

  const [scanned, setScanned] = useState(false);

  const [showPopupNotFound, setShowPopupNotFound] = useState(false);

  const [loading, setLoading] = useState(false);

  const handleRequestPermission = async () => {
    const permissionCamera = await BarCodeScanner.requestPermissionsAsync();
    if (permissionCamera.status !== 'granted') {
      Linking.openSettings();
    }
    const permissionCameraSecond =
      await BarCodeScanner.requestPermissionsAsync();
    setHasPermissionCamera(permissionCameraSecond.status === 'granted');
  };

  const onClosePopupNotFound = () => setShowPopupNotFound(false);

  useEffect(() => {
    (async () => {
      const permissionCamera = await BarCodeScanner.requestPermissionsAsync();
      setHasPermissionCamera(permissionCamera.status === 'granted');
    })();
  }, []);

  const pickImage = async () => {
    const permissionLibrary =
      await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (permissionLibrary.status !== 'granted') {
      console.log('Not permission Library');
      Linking.openSettings();
      return;
    }
    // setLoading(true);
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 1,
      });

      const res = await BarCodeScanner.scanFromURLAsync(result.uri);
      const data = res?.[0]?.data;
      if (data) {
        handleScanQr(data);
      } else {
        setShowPopupNotFound(true);
      }
    } catch (error) {
      console.log(error.message);
    } finally {
      setLoading(false);
    }
  };

  const handleBarCodeScanned = ({ type, data }) => {
    if (data) {
      if (scanned) {
        return;
      }
      setScanned(true);
      handleScanQr(data);
    }
  };

  const handleScanQr = async (qr) => {
    const responseDetectQrCode = await detectQRCode(qr);
    if (responseDetectQrCode.status) {
      if (
        responseDetectQrCode.response?.OpenEWallet?.Payment?.Detect?.succeeded
      ) {
        const data =
          responseDetectQrCode.response?.OpenEWallet?.Payment?.Detect ?? {};
        handlePayScanQr(data);
      } else {
        setShowPopupNotFound(true);
      }
    } else {
      console.log(
        responseDetectQrCode.response?.message ?? 'DetectQrCode Fail!'
      );
    }
  };

  const handleClose = () => {
    if (onClose) {
      onClose();
    }
  };

  if (hasPermissionCamera === null) {
    return null;
  }
  if (hasPermissionCamera === false) {
    return (
      <NotPermissionScreen
        handleClose={handleClose}
        handleRequestPermission={handleRequestPermission}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Camera
        style={styles.camera}
        type={Camera.Constants.Type.back}
        onBarCodeScanned={handleBarCodeScanned}
        barCodeScannerSettings={{
          barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],
        }}
        autoFocus={Camera.Constants.AutoFocus.on}
        flashMode={Camera.Constants.FlashMode.on}
        ratio="16:9"
      >
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
          <BarcodeMask
            edgeColor="#078f00"
            animatedLineColor="#078f00"
            edgeBorderWidth={2}
            width={widthBarcode}
            height={widthBarcode}
          />

          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
            }}
          >
            <IconButton
              icon={
                <Image
                  source={Images.iconBackWhite}
                  style={{ width: 32, height: 32 }}
                />
              }
              style={{ position: 'absolute', left: 10 }}
              onPress={handleClose}
            />
            <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold' }}>
              {getTranslate('ScanQR', lang)}
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              width: '76%',
              alignSelf: 'center',
              marginBottom: 20,
            }}
          >
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{
                  borderRadius: 50,
                  padding: 10,
                  backgroundColor: 'rgba(0,0,0,0.8)',
                  marginBottom: 5,
                  width: 50,
                  height: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={pickImage}
              >
                <Image source={require('../images/photo.png')} />
              </TouchableOpacity>
              <Text
                style={{ color: 'white', fontSize: 16, fontWeight: 'bold' }}
              >
                {getTranslate('ChoosePhoto', lang)}
              </Text>
            </View>

            {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{
                  borderRadius: 50,
                  padding: 10,
                  backgroundColor: 'rgba(0,0,0,0.8)',
                  marginBottom: 5,
                  width: 50,
                  height: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image source={require('../images/flash.png')} />
              </TouchableOpacity>
              <Text
                style={{ color: 'white', fontSize: 16, fontWeight: 'bold' }}
              >
                Bật flash
              </Text>
            </View> */}
          </View>
        </View>
      </Camera>

      {loading && (
        <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.8)',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <ActivityIndicator size="large" color="green" />
        </View>
      )}

      {showPopupNotFound && (
        <PopupScanQrNotFound
          mainColor={mainColor}
          onClose={onClosePopupNotFound}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    paddingTop: Platform.OS === 'ios' ? Constants.statusBarHeight : 0,
  },
  camera: {
    flex: 1,
  },
});
