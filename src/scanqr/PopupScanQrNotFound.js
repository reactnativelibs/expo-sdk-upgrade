/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Images } from '../images';
import { IconButton, Button } from '../components';

export const PopupScanQrNotFound = ({ mainColor, onClose = () => null }) => {
  return (
    <View style={styles.container}>
      <View style={styles.popupContainer}>
        <View style={styles.header}>
          <IconButton
            icon={
              <Image
                source={Images.iconClose}
                style={{ width: 16, height: 16 }}
              />
            }
            onPress={onClose}
          />
        </View>
        <View style={styles.image}>
          <Image
            source={Images.iconQrScanNotFound}
            style={{ width: 120, height: 121 }}
          />
        </View>
        <Text style={styles.txtNotFont}>Không tìm thấy</Text>
        <Text style={styles.subContentNotFont}>
          QRCode không đúng định dạng hoặc không tồn tại. Vui lòng kiểm tra và
          quét lại
        </Text>
        <Button
          text="Đóng"
          onPress={onClose}
          styles={styles.btnClose}
          mainColor={mainColor}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.8)',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  popupContainer: {
    width: '96%',
    padding: 12,
    borderRadius: 20,
    backgroundColor: 'white',
    marginBottom: 10,
  },
  header: {
    width: '100%',
    alignItems: 'flex-end',
  },
  txtNotFont: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    paddingVertical: 10,
  },
  subContentNotFont: {
    textAlign: 'center',
    marginBottom: 10,
  },
  image: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: 10,
  },
});
