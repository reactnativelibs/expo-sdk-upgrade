import { vi } from './locales/vi-VN';
import { en } from './locales/en-US';
import { LANGUAGES } from './../constants/index';

export const getTranslate = (key, lang = LANGUAGES.VI) => {
  const text = lang === LANGUAGES.VI ? vi[key] : en[key];
  return text ?? '--';
};
