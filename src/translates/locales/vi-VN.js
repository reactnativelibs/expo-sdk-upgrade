export const vi = {
  Close: 'Đóng',
  noPermission1: 'Cho phép truy cập máy ảnh của bạn',
  noPermission2:
    'Bạn hãy cho phép Payme truy cập thư máy ảnh trong phần Cài đặt của hệ thống để tiếp tục',
  noPermission4: 'Đã hiểu', //'ĐÃ HIỂU'
  completed: 'Hoàn tất',
  continute: 'Tiếp tục', //"TIẾP TỤC "
  ConfirmPhoto: 'Xác nhận ảnh',
  ConfirmVideo: 'Xác nhận video',
  textDecriptionKyc1:
    'Vui lòng xác nhận video đã rõ nét, gương mặt và giấy tờ tùy thân của bạn đã có trong khung hình.',
  textDecriptionKyc2:
    'Vui lòng xác nhận ảnh  đã rõ ràng, gương mặt của bạn có trong khung hình.',
  captureAgain: 'Chụp lại',
  IdentityCard: 'Chứng minh nhân dân',
  CitizenID: 'Căn cước công dân',
  Passport: 'Hộ chiếu',
  Notification: 'Thông báo',
  FacePhotography: 'Chụp ảnh khuôn mặt',
  AuthenticateVideoRecording: 'Quay video xác thực',
  TakePhotosOfDocuments: 'Chụp ảnh giấy tờ',
  contentPopupConfirmPassport:
    'Hộ chiếu chỉ dùng để cung cấp định danh cho người nước ngoài',
  Agree: 'Đồng ý',
  Change: 'Thay đổi',
  Loading: 'Đang tải dữ liệu…',
  Waiting: 'Vui lòng chờ trong giây lát',
  Front: 'Mặt trước',
  Back: 'Mặt sau',
  ContentKycVideo:
    'Giữ gương mặt và mặt trước giấy tờ tuỳ thân trước ống kính máy quay',
  ContentKycFace: 'Vui lòng giữ gương mặt trong khung tròn',
  ContentKycImage: 'Vui lòng cân chỉnh giấy tờ tùy thân vào giữa khung',

  CheckBulletImg1: '1. Sử dụng giấy tờ tuỳ thân hợp lệ của chính bạn',
  CheckBulletImg2: '2. Phải đặt thẻ bên trong khung camera',
  CheckBulletImg3: '3. Không sử dụng bản sao của giấy tờ tuỳ thân',

  CheckBulletVideo1: '1. Video sẽ có độ dài 5 giây',
  CheckBulletVideo2:
    '2. Vui lòng giữ gương mặt và mặt trước giấy tờ tùy thân của bạn trước máy quay',

  CheckBulletFace1: '1. Vui lòng giữ gương mặt ở trong khung tròn',
  CheckBulletFace2: '2. Giữ cho ảnh sắc nét không bị nhoè',

  TitleKycImage: 'Chụp hình giấy tờ tuỳ thân',
  TitleKycFace: 'Chụp ảnh xác thực khuôn mặt',
  TitleKycVideo: 'Quay video xác thực',

  SubtitleKyc:
    'Theo quy định của NHNN, tài khoản Ví điện tử phải được định danh trước khi sử dụng.',

  NotFound: 'Không tìm thấy',
  ContentNotFoundQRCode:
    'QRCode không đúng định dạng hoặc không tồn tại. Vui lòng kiểm tra và quét lại',

  ScanQR: 'Quét QR',
  ChoosePhoto: 'Chọn ảnh',
  MessageErrorPhoneMismatched: 'Số điện thoại không khớp với số đã đăng kí!',
  MessageError1: 'Có lỗi từ máy chủ hệ thống',
  MessageError2: 'Có lỗi xảy ra',
  MessageError404: 'Thông tin  xác thực không hợp lệ',
  MessageErrorBalance: 'Số dư ví PayME không đủ',
  MessageErrorNetwork: 'Lỗi kết nối mạng!',
};
